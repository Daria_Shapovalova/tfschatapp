package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class TopicWithUnreadCountNetwork(
    @SerialName("stream_id")
    val streamId: Int,

    @SerialName("topic")
    val topicName: String,

    @SerialName("unread_message_ids")
    val unreadMessageIds: List<Int>
)