package com.example.tfschatapp.network.modelnetwork

import com.example.tfschatapp.R
import com.example.tfschatapp.database.modeldb.MessageDb
import com.example.tfschatapp.database.modeldb.MessageWithReactions
import com.example.tfschatapp.database.modeldb.UserDb
import com.example.tfschatapp.presentation.extensions.parseHtml
import com.example.tfschatapp.presentation.modelui.DateUi
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.modelui.UserUi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@Serializable
class MessageNetwork(
    @SerialName("id")
    val uid: Int,

    @SerialName("timestamp")
    val timeStamp: Int,

    @SerialName("sender_id")
    val senderId: Int,

    @SerialName("sender_full_name")
    val senderName: String,

    @SerialName("sender_email")
    val senderEmail: String,

    @SerialName("avatar_url")
    val avatarUrl: String,

    @SerialName("content")
    val content: String,

    @SerialName("subject")
    val topic: String,

    @SerialName("reactions")
    val reactions: List<ReactionNetwork>
) {
    fun toMessageWithReactions(streamName: String, topicName: String): MessageWithReactions {
        val messageWithReactions = MessageWithReactions()

        val uid = uid.toString()

        val dateFormat: DateFormat = SimpleDateFormat("d MMM", Locale("ru"))
        val date = dateFormat.format(Date(timeStamp.toLong() * 1000))

        val senderId = senderId.toString()
        val content = content.parseHtml()

        val topicId = "$streamName $topic"

        messageWithReactions.message = MessageDb(uid, date, senderId, senderName, senderEmail, avatarUrl, content, topicId, topicName, streamName)
        messageWithReactions.reactions = reactions.map { reaction -> reaction.toReactionDatabase(uid) }

        return messageWithReactions
    }

    fun toMessageUi(topicId: String, topicName: String, currentUserId: String): MessageUi {
        val uid = uid.toString()

        val dateFormat: DateFormat = SimpleDateFormat("d MMM", Locale("ru"))
        val date = dateFormat.format(Date(timeStamp.toLong() * 1000))

        val sender = UserUi(senderId.toString(), senderName, senderEmail, avatarUrl)
        val content = content.parseHtml()

        val viewType = if (senderId.toString() == currentUserId) {
            R.layout.outgoing_message_item
        } else {
            R.layout.incoming_message_item
        }

        return MessageUi(uid, DateUi(date = date), sender, content, topicId = topicId, topicName = topicName, viewType = viewType)
    }
}