package com.example.tfschatapp.network.modelnetwork

class SendMessageRequest(
    val streamName: String,
    val topicName: String,
    val messageContent: String
)