package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class RegisterEventQueueResponse(
    @SerialName("queue_id")
    val queueId: String,

    @SerialName("last_event_id")
    val lastEventId: Int,

    @SerialName("unread_msgs")
    val unreadMessages: GetUnreadMessagesResponse? = null
)