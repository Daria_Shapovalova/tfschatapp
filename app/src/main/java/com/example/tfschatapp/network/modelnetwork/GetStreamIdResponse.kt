package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetStreamIdResponse(
    @SerialName("stream_id")
    val streamId: Int
)