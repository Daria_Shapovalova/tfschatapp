package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetTopicsResponse(
    @SerialName("topics")
    val topics: List<TopicNetwork>
)