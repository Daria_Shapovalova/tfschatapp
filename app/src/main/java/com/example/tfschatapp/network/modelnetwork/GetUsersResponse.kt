package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetUsersResponse(
    @SerialName("members")
    val members: List<UserNetwork>
)