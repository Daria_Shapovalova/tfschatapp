package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetOwnUserResponse(
    @SerialName("user_id")
    val userId: Int
)