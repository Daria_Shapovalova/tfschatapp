package com.example.tfschatapp.network.modelnetwork

import com.example.tfschatapp.database.modeldb.UserDb
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class UserNetwork(
    @SerialName("user_id")
    val uid: Int,

    @SerialName("full_name")
    val name: String,

    @SerialName("email")
    val email: String,

    @SerialName("avatar_url")
    val avatarUrl: String
) {
    fun toUserDb(): UserDb {
        return UserDb(uid.toString(), name, email, avatarUrl)
    }
}