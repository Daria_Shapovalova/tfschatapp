package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetSubscriptionsResponse(
    @SerialName("subscriptions")
    val subscriptions: List<StreamNetwork>
)