package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetMessagesResponse(
    @SerialName("found_oldest")
    val foundOldest: Boolean,

    @SerialName("messages")
    val messages: List<MessageNetwork>
)