package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetEventsFromQueueResponse(
    @SerialName("result")
    val result: String,

    @SerialName("events")
    val events: List<EventNetwork>? = null
)