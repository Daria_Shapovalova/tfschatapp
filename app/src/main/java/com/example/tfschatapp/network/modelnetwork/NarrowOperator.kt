package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class NarrowOperator(
    @SerialName("operator")
    val operator: String,

    @SerialName("operand")
    val operand: String,
)