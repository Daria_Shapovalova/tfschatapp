package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class ReportUserPresenceResponse(
    @SerialName("presences")
    val presences: Map<String, UserPresenceNetwork>
)