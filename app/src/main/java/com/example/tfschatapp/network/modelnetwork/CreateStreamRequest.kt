package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class CreateStreamRequest(
    @SerialName("name")
    val streamName: String
)