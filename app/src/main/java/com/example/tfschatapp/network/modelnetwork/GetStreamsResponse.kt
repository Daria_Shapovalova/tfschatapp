package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetStreamsResponse(
    @SerialName("streams")
    val streams: List<StreamNetwork>
)