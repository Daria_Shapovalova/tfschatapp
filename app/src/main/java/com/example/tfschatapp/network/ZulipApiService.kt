package com.example.tfschatapp.network

import com.example.tfschatapp.network.modelnetwork.*
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import kotlinx.serialization.json.Json
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

private const val BASE_URL = "https://tfs-android-2021-spring.zulipchat.com/api/v1/"
private const val READ_TIMEOUT_SECONDS: Long = 60
private const val AUTHORIZATION_HEADER = "Authorization"
private val contentType = "application/json".toMediaType()
private val basicCredentials = Credentials.basic(
    username = "daria.shapovalova.omsu@gmail.com",
    password = "ToxFZLhkf5R7nhNm9TBAVw5P91NWqaZZ"
)

val authenticationInterceptor = Interceptor { chain ->
    val request: Request = chain.request()
    val authenticatedRequest: Request = request.newBuilder()
        .header(
            AUTHORIZATION_HEADER,
            basicCredentials
        ).build()
    chain.proceed(authenticatedRequest)
}

val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
    setLevel(HttpLoggingInterceptor.Level.BODY)
}

private val okHttpClient = OkHttpClient.Builder()
    .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
    .addInterceptor(authenticationInterceptor)
    .addInterceptor(httpLoggingInterceptor)
    .build()

private val retrofitBuilder: Retrofit.Builder = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(Json { ignoreUnknownKeys = true }.asConverterFactory(contentType))
    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
    .client(okHttpClient)

private val retrofit = retrofitBuilder.build()

interface ZulipApiService {
    @GET("users")
    fun getAllUsers(): Single<GetUsersResponse>

    @GET("users/me")
    fun getOwnUser(): Single<GetOwnUserResponse>

    @GET("streams")
    fun getAllStreams(): Single<GetStreamsResponse>

    @GET("users/me/subscriptions")
    fun getSubscribedStreams(): Single<GetSubscriptionsResponse>

    @GET("users/me/{stream_id}/topics")
    fun getTopicsInStream(@Path("stream_id") streamId: Int): Observable<GetTopicsResponse>

    @GET("messages")
    fun getMessages(
        @Query("anchor") anchor: Long,
        @Query("num_before") numBefore: Int = MESSAGES_PAGE_SIZE,
        @Query("num_after") numAfter: Int = 0,
        @Query("narrow") narrow: String
    ): Single<GetMessagesResponse>

    @FormUrlEncoded
    @POST("mark_topic_as_read")
    fun markMessagesAsReadInTopic(
        @Field("stream_id") streamId: Int,
        @Field("topic_name") topicName: String
    ): Completable

    @FormUrlEncoded
    @POST("mark_stream_as_read")
    fun markMessagesAsReadInStream(
        @Field("stream_id") streamId: Int
    ): Completable

    @FormUrlEncoded
    @POST("messages")
    fun sendMessage(
        @Field("type") type: String = "stream",
        @Field("to") streamName: String,
        @Field("content") content: String,
        @Field("topic") topicName: String
    ): Single<SendMessageResponse>

    @FormUrlEncoded
    @POST("messages/{message_id}/reactions")
    fun addReaction(
        @Path("message_id") messageId: Int,
        @Field("emoji_name") emojiName: String
    ): Completable

    @DELETE("messages/{message_id}/reactions")
    fun removeReaction(
        @Path("message_id") messageId: Int,
        @Query("emoji_name") emojiName: String
    ): Completable

    @FormUrlEncoded
    @POST("users/me/presence")
    fun reportUserPresence(
        @Field("status") status: String = "active"
    ): Single<ReportUserPresenceResponse>

    @FormUrlEncoded
    @POST("register")
    fun registerEventQueue(
        @Field("apply_markdown") applyHtmlMarkdown: Boolean = false,
        @Field("event_types") eventTypes: String,
        @Field("narrow") narrow: String? = null
    ): Single<RegisterEventQueueResponse>

    @GET("events")
    fun getEventsFromQueue(
        @Query("queue_id") queueId: String,
        @Query("last_event_id") lastEventId: Int
    ): Single<GetEventsFromQueueResponse>

    @DELETE("messages/{message_id}")
    fun deleteMessage(
        @Path("message_id") messageId: Int
    ):  Completable

    @FormUrlEncoded
    @PATCH("messages/{message_id}")
    fun editMessage(
        @Path("message_id") messageId: Int,
        @Field("topic") topicName: String,
        @Field("content") content: String
    ): Completable

    @FormUrlEncoded
    @POST("users/me/subscriptions")
    fun createStream(
        @Field("subscriptions") createStreamRequest: String
    ): Completable

    @GET("get_stream_id")
    fun getStreamId(
        @Query("stream") streamName: String
    ): Single<GetStreamIdResponse>

    companion object {
        private const val MESSAGES_PAGE_SIZE = 20
    }
}

class ZulipApi {
    val retrofitService: ZulipApiService by lazy {
        retrofit.create(ZulipApiService::class.java)
    }
}