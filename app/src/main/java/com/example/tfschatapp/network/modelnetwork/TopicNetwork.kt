package com.example.tfschatapp.network.modelnetwork

import com.example.tfschatapp.database.modeldb.TopicDb
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class TopicNetwork(
    @SerialName("name")
    val name: String
) {
    fun toTopicDatabase(streamId: Int): TopicDb {
        return TopicDb("$streamId $name", name, streamId.toString())
    }
}