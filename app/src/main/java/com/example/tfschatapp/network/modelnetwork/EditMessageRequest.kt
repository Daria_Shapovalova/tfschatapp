package com.example.tfschatapp.network.modelnetwork

class EditMessageRequest(
    val messageId: String,
    val topicName: String,
    val messageContent: String
)