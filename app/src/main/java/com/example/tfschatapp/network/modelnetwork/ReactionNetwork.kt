package com.example.tfschatapp.network.modelnetwork

import com.example.tfschatapp.database.modeldb.ReactionDb
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class ReactionNetwork(
    @SerialName("emoji_code")
    val emojiCode: String,

    @SerialName("emoji_name")
    val emojiName: String,

    @SerialName("user_id")
    val userId: Int
) {
    fun toReactionDatabase(messageId: String): ReactionDb {
        val emoji = String(Character.toChars(emojiCode.toInt(radix = 16)))
        return ReactionDb("$emojiName $userId", emojiName, emoji, messageId, userId.toString())
    }
}