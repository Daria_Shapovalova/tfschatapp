package com.example.tfschatapp.network.modelnetwork

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AddOrRemoveReactionRequest(
    val messageId: Int,
    val emojiName: String
) : Parcelable