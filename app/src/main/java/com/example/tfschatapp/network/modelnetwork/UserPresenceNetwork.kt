package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class UserPresenceNetwork(
    @SerialName("aggregated")
    val aggregated: UserStatusNetwork
)