package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class UserStatusNetwork(
    @SerialName("status")
    val status: String,

    @SerialName("timestamp")
    val timestamp: Int
)