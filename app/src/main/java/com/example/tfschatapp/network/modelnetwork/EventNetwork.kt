package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class EventNetwork(
    @SerialName("id")
    val id: Int,

    @SerialName("type")
    val type: String,

    @SerialName("op")
    val operation: String? = null,

    @SerialName("message")
    val message: MessageNetwork? = null,

    @SerialName("user_id")
    val userId: Int? = null,

    @SerialName("message_id")
    val messageId: Int? = null,

    @SerialName("emoji_name")
    val emojiName: String? = null,

    @SerialName("emoji_code")
    val emojiCode: String? = null,

    @SerialName("rendered_content")
    val newMessageContent: String? = null,

    @SerialName("subject")
    val newMessageTopic: String? = null
)