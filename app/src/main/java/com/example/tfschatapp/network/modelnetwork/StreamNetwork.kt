package com.example.tfschatapp.network.modelnetwork

import com.example.tfschatapp.database.modeldb.StreamDb
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class StreamNetwork(
    @SerialName("stream_id")
    val uid: Int,

    @SerialName("name")
    val name: String
) {
    fun toStreamDatabase(): StreamDb {
        return StreamDb(uid.toString(), name)
    }
}