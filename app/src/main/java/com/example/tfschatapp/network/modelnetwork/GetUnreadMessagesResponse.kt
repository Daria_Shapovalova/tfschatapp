package com.example.tfschatapp.network.modelnetwork

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class GetUnreadMessagesResponse(
    @SerialName("streams")
    val streams: List<TopicWithUnreadCountNetwork>? = null
)