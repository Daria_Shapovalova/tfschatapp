package com.example.tfschatapp.presentation.recyclerview.holders

import android.view.View
import com.example.tfschatapp.databinding.ItemTopicHeaderBinding
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.recyclerview.BaseViewHolder

class TopicHeaderViewHolder(val view: View, private val topicClick: TopicClickListener? = null) : BaseViewHolder<TopicUi>(view) {
    private var topicViewBinding: ItemTopicHeaderBinding = ItemTopicHeaderBinding.bind(view)

    override fun bind(item: TopicUi) {
        topicViewBinding.topic = item
        topicViewBinding.clickListener = topicClick
    }
}