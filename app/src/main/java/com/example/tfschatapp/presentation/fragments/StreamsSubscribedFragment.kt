package com.example.tfschatapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.tfschatapp.databinding.FragmentSubscribedChannelsBinding
import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.modelui.ViewTyped
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.MviViewWithUiEffects
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.presentation.mvi.uieffects.StreamsUiEffect
import com.example.tfschatapp.presentation.recyclerview.AsyncAdapter
import com.example.tfschatapp.presentation.recyclerview.ChatHolderFactory
import com.example.tfschatapp.presentation.recyclerview.holders.StreamClickListener
import com.example.tfschatapp.presentation.recyclerview.holders.TopicClickListener
import com.example.tfschatapp.presentation.viewmodels.StreamsViewModel
import com.jakewharton.rxrelay3.PublishRelay

class StreamsSubscribedFragment : Fragment(), MviViewWithUiEffects<StreamsAction, StreamsState, StreamsUiEffect> {
    override val actions: PublishRelay<StreamsAction> = PublishRelay.create()
    private lateinit var viewModel: StreamsViewModel

    private var _binding: FragmentSubscribedChannelsBinding? = null
    private val binding get() = _binding!!

    private lateinit var showTopicsForStreamClickListener: StreamClickListener
    private lateinit var navigateToStreamClickListener: StreamClickListener
    private lateinit var navigateToTopicClickListener: TopicClickListener

    private lateinit var holderFactory: ChatHolderFactory
    private lateinit var adapter: AsyncAdapter<ViewTyped>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSubscribedChannelsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpClickListeners()
        setUpRecyclerView()

        viewModel = (parentFragment as StreamsMainFragment).viewModel
        viewModel.bind(this)
    }

    private fun setUpClickListeners() {
        showTopicsForStreamClickListener = StreamClickListener { stream -> actions.accept(StreamsAction.OpenOrCloseStreamAction(stream)) }
        navigateToStreamClickListener = StreamClickListener { stream -> navigateToSelectedStream(stream) }
        navigateToTopicClickListener = TopicClickListener { topic -> navigateToSelectedTopic(topic) }
    }

    private fun setUpRecyclerView() {
        holderFactory = ChatHolderFactory(streamNavigateClick = navigateToStreamClickListener, streamShowTopicsClick = showTopicsForStreamClickListener,
            topicClick = navigateToTopicClickListener)
        adapter = AsyncAdapter(holderFactory)
        binding.channelsRecyclerView.adapter = adapter
    }

    private fun hideShimmerPlaceholder() {
        binding.shimmerPlaceholderLayout.stopShimmer()
        binding.shimmerPlaceholderLayout.visibility = View.GONE
    }

    private fun navigateToSelectedStream(selectedStream: StreamUi?) {
        if (selectedStream != null) {
            this.findNavController().navigate(StreamsMainFragmentDirections.actionShowStreamDiscussion(selectedStream))
        }
    }

    private fun navigateToSelectedTopic(selectedTopic: TopicUi?) {
        if (selectedTopic != null) {
            this.findNavController().navigate(StreamsMainFragmentDirections.actionShowTopicDiscussion(selectedTopic))
        }
    }

    override fun render(state: StreamsState) {
        if (!state.isLoading && state.subscribedFilteredStreamsWithTopics.isNotEmpty()) {
            hideShimmerPlaceholder()
        }
        adapter.items = state.subscribedFilteredStreamsWithTopics
    }

    override fun handleUiEffect(uiEffect: StreamsUiEffect?) {}

    override fun onDestroyView() {
        viewModel.unbind()
        super.onDestroyView()
    }
}