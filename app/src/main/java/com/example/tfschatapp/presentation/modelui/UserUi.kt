package com.example.tfschatapp.presentation.modelui

import android.os.Parcelable
import com.example.tfschatapp.R
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class UserUi(
    override val uid: String,
    val name: String,
    val email: String,
    val avatarUrl: String,
    val presence: String = "offline",
    override val viewType: Int = R.layout.item_contact
) : ViewTyped, Parcelable