package com.example.tfschatapp.presentation.mvi.actions

import com.example.tfschatapp.database.modeldb.MessageWithReactions
import com.example.tfschatapp.network.modelnetwork.*
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.modelui.UserUi

sealed class MessagesAction {
    data class ShowCachedMessagesForTopicAction(val topic: TopicUi) : MessagesAction()
    data class ShowCachedMessagesForStreamAction(val stream: StreamUi) : MessagesAction()
    data class ShowCachedMessagesSuccessAction(val messagesUi: List<MessageUi>) : MessagesAction()
    data class ShowCachedMessagesFailureAction(val error: Throwable) : MessagesAction()

    object LoadFirstMessagesPageAction : MessagesAction()
    data class LoadFirstMessagesPageSuccessAction(val lastMessageFound: Boolean, val messagesUi: List<MessageUi>, val messagesDb: List<MessageWithReactions>) :
        MessagesAction()

    data class LoadFirstMessagesPageFailureAction(val error: Throwable) : MessagesAction()

    object LoadNextMessagesPageAction : MessagesAction()
    data class LoadNextMessagesPageSuccessAction(val lastMessageFound: Boolean, val messages: List<MessageUi>, val messagesDb: List<MessageWithReactions>) :
        MessagesAction()

    data class LoadNextMessagesPageFailureAction(val error: Throwable) : MessagesAction()

    data class CacheMessagesAction(val messagesCachingMode: MessagesCachingMode, val messages: List<MessageWithReactions>) : MessagesAction()

    object ScrolledToLastMessageAction : MessagesAction()

    data class SendMessageAction(val sendMessageRequest: SendMessageRequest) : MessagesAction()
    object SendMessageSuccessAction : MessagesAction()
    data class SendMessageFailureAction(val error: Throwable) : MessagesAction()

    object RegisterEventQueueAction : MessagesAction()
    data class RegisterEventQueueSuccessAction(
        val streamName: String,
        val topicName: String? = null,
        val registerEventQueueResponse: RegisterEventQueueResponse
    ) : MessagesAction()

    data class RegisterEventQueueFailureAction(val error: Throwable) : MessagesAction()

    data class GetEventsFromQueueAction(val streamName: String, val topicName: String? = null, val queueId: String, val lastEventId: Int) : MessagesAction()
    data class GetEventsFromQueueSuccessAction(val topic: TopicUi, val queueId: String, val lastEventId: Int) : MessagesAction()
    data class GetEventsFromQueueFailureAction(val error: Throwable) : MessagesAction()

    data class ParseEventsAction(val streamName: String, val topicName: String? = null, val queueId: String, val events: List<EventNetwork>) : MessagesAction()
    data class ParseMessageFromEventAction(val event: EventNetwork, val streamName: String, val topicName: String? = null) : MessagesAction()

    data class AddReactionAction(val request: AddOrRemoveReactionRequest) : MessagesAction()
    object AddReactionSuccessAction : MessagesAction()
    data class AddReactionFailureAction(val error: Throwable) : MessagesAction()

    data class RemoveReactionAction(val request: AddOrRemoveReactionRequest) : MessagesAction()
    object RemoveReactionSuccessAction : MessagesAction()
    data class RemoveReactionFailureAction(val error: Throwable) : MessagesAction()

    data class ParseReactionFromEventAction(val event: EventNetwork) : MessagesAction()
    object ParseReactionFromEventSuccessAction : MessagesAction()
    data class ParseReactionFromEventFailureAction(val error: Throwable) : MessagesAction()

    data class MarkAllMessagesInTopicAsReadAction(val topic: TopicUi) : MessagesAction()
    data class MarkAllMessagesInStreamAsReadAction(val stream: StreamUi) : MessagesAction()
    object MarkAllMessagesAsReadSuccessAction : MessagesAction()
    data class MarkAllMessagesAsReadFailureAction(val error: Throwable) : MessagesAction()

    data class MarkCachedMessagesInTopicAsReadAction(val topic: TopicUi) : MessagesAction()
    data class MarkCachedMessagesInStreamAsReadAction(val stream: StreamUi) : MessagesAction()
    object MarkCachedMessagesAsReadSuccessAction : MessagesAction()
    data class MarkCachedMessagesAsReadFailureAction(val error: Throwable) : MessagesAction()

    object ScrollToBottomAction : MessagesAction()

    data class NavigateToUserProfile(val user: UserUi) : MessagesAction()

    data class SelectReactionAction(val messageId: String) : MessagesAction()

    data class CopyToClipboardAction(val messageText: String) : MessagesAction()

    data class OpenMessageEditorAction(val message: MessageUi) : MessagesAction()

    data class DeleteMessageAction(val messageId: String) : MessagesAction()
    object DeleteMessageSuccessAction : MessagesAction()
    data class DeleteMessageFailureAction(val error: Throwable) : MessagesAction()

    data class ParseDeletedMessageFromEventAction(val messageId: String) : MessagesAction()
    object ParseDeletedMessageFromEventSuccessAction : MessagesAction()
    data class ParseDeletedMessageFromEventFailureAction(val error: Throwable) : MessagesAction()

    data class EditMessageAction(val editMessageRequest: EditMessageRequest) : MessagesAction()
    object EditMessageSuccessAction : MessagesAction()
    data class EditMessageFailureAction(val error: Throwable) : MessagesAction()

    data class ParseEditedMessageContentFromEventAction(val messageId: String, val newMessageContent: String) : MessagesAction()
    data class ParseEditedMessageTopicFromEventAction(val messageId: String, val newMessageTopic: String) : MessagesAction()
    object ParseEditedMessageFromEventSuccessAction : MessagesAction()
    data class ParseEditedMessageFromEventFailureAction(val error: Throwable) : MessagesAction()
}

enum class MessagesCachingMode {
    CACHE_TOPIC, CACHE_STREAM
}