package com.example.tfschatapp.presentation.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.example.tfschatapp.presentation.modelui.ViewTyped

class DiffCallback : DiffUtil.ItemCallback<ViewTyped>() {
    override fun areItemsTheSame(oldItem: ViewTyped, newItem: ViewTyped): Boolean {
        return oldItem.uid == newItem.uid
    }

    override fun areContentsTheSame(oldItem: ViewTyped, newItem: ViewTyped): Boolean {
        return oldItem == newItem
    }
}