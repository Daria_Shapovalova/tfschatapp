package com.example.tfschatapp.presentation.managers

import android.graphics.Color

object ColorManager {
    private val colors = listOf(
        Color.parseColor("#d32f2f"),
        Color.parseColor("#c2185b"),
        Color.parseColor("#7b1fa2"),
        Color.parseColor("#512da8"),
        Color.parseColor("#303f9f"),
        Color.parseColor("#1976d2"),
        Color.parseColor("#0288d1"),
        Color.parseColor("#0097a7"),
        Color.parseColor("#00796b"),
        Color.parseColor("#388e3c"),
        Color.parseColor("#689f38"),
        Color.parseColor("#afb42b"),
        Color.parseColor("#f57c00"),
        Color.parseColor("#e64a19"),
        Color.parseColor("#e53935"),
        Color.parseColor("#d81b60"),
        Color.parseColor("#8e24aa"),
        Color.parseColor("#5e35b1"),
        Color.parseColor("#3949ab"),
        Color.parseColor("#1e88e5"),
        Color.parseColor("#039be5"),
        Color.parseColor("#00acc1"),
        Color.parseColor("#00897b"),
        Color.parseColor("#43a047"),
        Color.parseColor("#7cb342"),
        Color.parseColor("#ff6f00")
    )

    fun getRandomColor(): Int {
        return colors.shuffled()[0]
    }
}