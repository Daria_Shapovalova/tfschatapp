package com.example.tfschatapp.presentation.mvi.actions

import com.example.tfschatapp.database.modeldb.StreamDb
import com.example.tfschatapp.database.modeldb.TopicDb
import com.example.tfschatapp.presentation.modelui.StreamUi

sealed class StreamsAction {
    object LoadStreamsFromNetworkAction : StreamsAction()
    data class LoadStreamsFromNetworkSuccessAction(val streams: List<StreamDb>) : StreamsAction()
    data class LoadStreamsFromNetworkFailureAction(val error: Throwable) : StreamsAction()

    data class CacheStreamsAction(val streams: List<StreamDb>) : StreamsAction()
    data class CacheStreamsFailureAction(val error: Throwable) : StreamsAction()

    object LoadSubscribedStreamsIdsAction : StreamsAction()
    data class LoadSubscribedStreamsIdsFailureAction(val error: Throwable) : StreamsAction()

    data class MarkStreamsAsSubscribedAction(val streamIds: List<String>) : StreamsAction()
    object MarkStreamsAsSubscribedSuccessAction : StreamsAction()
    data class MarkStreamsAsSubscribedFailureAction(val error: Throwable) : StreamsAction()

    data class LoadTopicsFromNetworkAction(val streams: List<StreamDb>) : StreamsAction()
    data class LoadTopicsFromNetworkFailureAction(val error: Throwable) : StreamsAction()

    data class CacheTopicsAction(val topics: List<TopicDb>) : StreamsAction()
    data class CacheTopicsFailureAction(val error: Throwable) : StreamsAction()

    object LoadUnreadMessageCountsAction : StreamsAction()
    data class LoadUnreadMessageCountsFailureAction(val error: Throwable) : StreamsAction()

    data class CacheUnreadMessageCountsAction(val unreadCounts: Map<String, Int>) : StreamsAction()
    object CacheUnreadMessageCountsSuccessAction : StreamsAction()
    data class CacheUnreadMessageCountsFailureAction(val error: Throwable) : StreamsAction()

    object LoadStreamsFromCacheAction : StreamsAction()
    data class LoadStreamsFromCacheSuccessAction(val streams: Pair<List<StreamUi>, List<StreamUi>>) : StreamsAction()
    data class LoadStreamsFromCacheFailureAction(val error: Throwable) : StreamsAction()

    data class FilterStreamsByQueryAction(val searchQuery: String) : StreamsAction()

    data class OpenOrCloseStreamAction(val stream: StreamUi) : StreamsAction()
    object OpenOrCloseStreamSuccessAction : StreamsAction()
    data class OpenOrCloseStreamFailureAction(val error: Throwable) : StreamsAction()

    data class CreateNewStreamAction(val streamName: String) : StreamsAction()
    data class GetStreamIdAction(val streamName: String) : StreamsAction()
    data class CreateNewStreamSuccessAction(val stream: StreamUi) : StreamsAction()
    data class CreateNewStreamFailureAction(val error: Throwable) : StreamsAction()
    data class GetStreamIdFailureAction(val error: Throwable) : StreamsAction()
}