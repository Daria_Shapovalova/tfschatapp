package com.example.tfschatapp.presentation.modelui

import android.os.Parcelable
import com.example.tfschatapp.R
import com.example.tfschatapp.database.modeldb.StreamDb
import kotlinx.parcelize.Parcelize

@Parcelize
data class StreamUi(
    override val uid: String,
    val name: String,
    val isOpened: Boolean = false,
    val subscribed: Boolean = false,
    val topics: MutableList<TopicUi> = mutableListOf(),
    override val viewType: Int = R.layout.item_channel
) : ViewTyped, Parcelable {
    fun toStreamDb(): StreamDb {
        return StreamDb(uid, name, isOpened, subscribed)
    }

    fun toStreamWithTopicsUi(): List<ViewTyped> {
        val streamWithTopics: MutableList<ViewTyped> = mutableListOf()
        streamWithTopics.add(this)
        if (isOpened) {
            topics.forEach { topic ->
                streamWithTopics.add(topic)
            }
        }
        return streamWithTopics
    }
}