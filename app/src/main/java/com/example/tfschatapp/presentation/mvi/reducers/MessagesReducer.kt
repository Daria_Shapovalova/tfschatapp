package com.example.tfschatapp.presentation.mvi.reducers

import com.example.tfschatapp.database.modeldb.ReactionDb
import com.example.tfschatapp.presentation.extensions.parseHtml
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.mvi.Reducer
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class MessagesReducer : Reducer<MessagesState, MessagesAction> {
    override fun reduce(state: MessagesState, action: MessagesAction): MessagesState {
        return when (action) {
            is MessagesAction.ShowCachedMessagesForTopicAction -> state.copy(
                topic = action.topic
            )

            is MessagesAction.ShowCachedMessagesForStreamAction -> state.copy(
                stream = action.stream
            )

            is MessagesAction.ShowCachedMessagesSuccessAction -> state.copy(
                messages = action.messagesUi.sortedBy { it.uid }
            )

            is MessagesAction.LoadFirstMessagesPageAction -> state.copy(
                isLoading = true,
                error = null
            )

            is MessagesAction.LoadFirstMessagesPageSuccessAction -> state.copy(
                isLoading = false,
                messages = action.messagesUi,
                lastLoadedMessageId = action.messagesUi[0].uid.toLong() - 1,
                lastMessageFound = action.lastMessageFound
            )

            is MessagesAction.LoadFirstMessagesPageFailureAction -> state.copy(
                isLoading = false,
                error = action.error
            )

            is MessagesAction.LoadNextMessagesPageAction -> state.copy(
                isLoading = true
            )

            is MessagesAction.LoadNextMessagesPageSuccessAction -> {
                val messagesList = state.messages.toMutableList()
                messagesList.addAll(action.messages)
                state.copy(
                    isLoading = false,
                    messages = messagesList.sortedBy { it.uid },
                    lastLoadedMessageId = action.messages[0].uid.toLong() - 1,
                    lastMessageFound = action.lastMessageFound
                )
            }

            is MessagesAction.LoadNextMessagesPageFailureAction -> state.copy(
                isLoading = false,
                error = action.error
            )

            is MessagesAction.RegisterEventQueueSuccessAction -> state.copy(
                eventsQueueId = action.registerEventQueueResponse.queueId,
            )

            is MessagesAction.ParseMessageFromEventAction -> {
                val message = action.event.message!!
                val messageTopicId = if (state.topic != null) {
                    state.topic.uid
                } else {
                    "${state.stream!!.name} ${message.topic}"
                }
                val messageUi = message.toMessageUi(messageTopicId, message.topic, state.currentUserId!!)
                val messagesList = state.messages.toMutableList()
                messagesList.add(messageUi)
                state.copy(
                    messages = messagesList.sortedBy { it.uid }
                )
            }

            is MessagesAction.ParseReactionFromEventAction -> {
                val emoji = String(Character.toChars(action.event.emojiCode!!.toInt(radix = 16)))
                val reaction = ReactionDb(
                    uid = "${action.event.emojiName} ${action.event.userId}",
                    emojiName = action.event.emojiName!!,
                    emoji = emoji,
                    messageId = action.event.messageId.toString(),
                    userId = action.event.userId.toString()
                )
                val messagesList = state.messages.toMutableList()
                val message = messagesList.find { it.uid == reaction.messageId }
                if (message != null) {
                    val updatedMessage = Json.decodeFromString<MessageUi>(Json.encodeToString(message))
                    val reactionUi = updatedMessage.reactions.find { it.uid == reaction.emojiName }
                    reactionUi?.update(action.event.operation!!, reaction.userId == state.currentUserId)
                    if (reactionUi?.count == 0) {
                        val reactions = updatedMessage.reactions.toMutableList()
                        reactions.remove(reactionUi)
                        updatedMessage.reactions = reactions
                    }
                    if (reactionUi == null) {
                        updatedMessage.addNewReaction(
                            reaction = reaction,
                            fromCurrentUser = reaction.userId == state.currentUserId
                        )
                    }
                    messagesList.remove(message)
                    messagesList.add(updatedMessage)
                }
                state.copy(
                    messages = messagesList.sortedBy { it.uid }
                )
            }

            is MessagesAction.ParseDeletedMessageFromEventAction -> {
                val messagesList = state.messages.toMutableList()
                val messageToRemove = messagesList.find { it.uid == action.messageId }
                messagesList.remove(messageToRemove)
                state.copy(
                    messages = messagesList.sortedBy { it.uid }
                )
            }

            is MessagesAction.ParseEditedMessageContentFromEventAction -> {
                val messagesList = state.messages.toMutableList()
                val message = messagesList.find { it.uid == action.messageId }
                if (message != null) {
                    val updatedMessage = Json.decodeFromString<MessageUi>(Json.encodeToString(message))
                    updatedMessage.messageText = action.newMessageContent.parseHtml()
                    messagesList.remove(message)
                    messagesList.add(updatedMessage)
                }
                state.copy(
                    messages = messagesList.sortedBy { it.uid }
                )
            }

            is MessagesAction.ParseEditedMessageTopicFromEventAction -> {
                val messagesList = state.messages.toMutableList()
                val message = messagesList.find { it.uid == action.messageId }
                if (message != null) {
                    messagesList.remove(message)
                    if (state.stream != null) {
                        val updatedMessage = Json.decodeFromString<MessageUi>(Json.encodeToString(message))
                        updatedMessage.topicName = action.newMessageTopic
                        updatedMessage.topicId = "${state.stream.name} ${action.newMessageTopic}"
                        messagesList.add(updatedMessage)
                    }
                }
                state.copy(
                    messages = messagesList.sortedBy { it.uid }
                )
            }

            else -> state
        }
    }
}