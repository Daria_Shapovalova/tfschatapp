package com.example.tfschatapp.presentation.recyclerview

import android.view.View
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.recyclerview.holders.*

class ChatHolderFactory(
    private val messageLongClick: ((View) -> Boolean)? = null, private val reactionClick: ((View) -> Unit)? = null,
    private val addReactionClick: ((View) -> Unit)? = null, private val streamNavigateClick: StreamClickListener? = null,
    private val streamShowTopicsClick: StreamClickListener? = null, private val topicClick: TopicClickListener? = null,
    private val contactClick: ContactClickListener? = null, private val emojiClick: EmojiClickListener? = null
) : HolderFactory() {
    override fun createViewHolder(view: View, viewType: Int): BaseViewHolder<*>? {
        return when (viewType) {
            R.layout.incoming_message_item -> IncomingMessageViewGroupHolder(view, messageLongClick, reactionClick, addReactionClick, contactClick)
            R.layout.outgoing_message_item -> OutgoingMessageViewGroupHolder(view, messageLongClick, reactionClick, addReactionClick)
            R.layout.item_date -> DateViewHolder(view)
            R.layout.item_emoji -> EmojiViewHolder(view, emojiClick)
            R.layout.item_contact -> ContactViewHolder(view, contactClick)
            R.layout.item_channel -> ChannelViewHolder(view, streamNavigateClick, streamShowTopicsClick)
            R.layout.item_topic -> TopicViewHolder(view, topicClick)
            R.layout.item_topic_header -> TopicHeaderViewHolder(view, topicClick)
            else -> null
        }
    }
}
