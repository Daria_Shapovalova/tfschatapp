package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.actions.MessagesCachingMode
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import io.reactivex.rxjava3.core.Observable

class CacheFirstMessagesPageMiddleware : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.LoadFirstMessagesPageSuccessAction::class.java)
            .withLatestFrom(state) { action, currentState -> action to currentState }
            .flatMap { (action, state) ->
                val messagesCachingMode = if (state.topic != null) {
                    MessagesCachingMode.CACHE_TOPIC
                } else MessagesCachingMode.CACHE_STREAM
                Observable.just(MessagesAction.CacheMessagesAction(messagesCachingMode, action.messagesDb))
            }
    }
}