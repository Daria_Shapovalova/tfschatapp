package com.example.tfschatapp.presentation.recyclerview.holders

import android.view.View
import android.widget.TextView
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.modelui.DateUi
import com.example.tfschatapp.presentation.recyclerview.BaseViewHolder

class DateViewHolder(view: View) : BaseViewHolder<DateUi>(view) {
    private val dateHolder: TextView = view.findViewById(R.id.date)

    override fun bind(item: DateUi) {
        dateHolder.text = item.date
    }
}
