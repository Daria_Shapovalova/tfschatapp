package com.example.tfschatapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.tfschatapp.R
import com.example.tfschatapp.databinding.FragmentProfileBinding
import com.example.tfschatapp.presentation.activity.MainActivity
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.actions.ProfileAction
import com.example.tfschatapp.presentation.mvi.states.ProfileState
import com.example.tfschatapp.presentation.viewmodels.CurrentUserProfileViewModel
import com.jakewharton.rxrelay3.PublishRelay
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment : Fragment(), MviView<ProfileAction, ProfileState> {
    override val actions: PublishRelay<ProfileAction> = PublishRelay.create()
    private val viewModel: CurrentUserProfileViewModel by viewModels()

    private var _binding: FragmentProfileBinding? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.bind(this)
        actions.accept(ProfileAction.LoadCurrentUserProfileAction)
        setUpRetryButton()
    }

    override fun render(state: ProfileState) {
        if (state.userProfile == null) {
            binding.profileLayout.visibility = View.GONE
            binding.errorNetworkLayout.root.visibility = View.VISIBLE
        } else {
            binding.profileLayout.visibility = View.VISIBLE
            binding.errorNetworkLayout.root.visibility = View.GONE
            binding.contact = state.userProfile
        }
    }

    private fun setUpRetryButton() {
        binding.errorNetworkLayout.networkErrorRetryButton.setOnClickListener {
            (activity as MainActivity).loadUsersData()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        viewModel.unbind()
    }
}