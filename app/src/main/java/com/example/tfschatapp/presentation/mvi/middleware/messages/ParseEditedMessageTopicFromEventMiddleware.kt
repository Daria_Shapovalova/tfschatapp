package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class ParseEditedMessageTopicFromEventMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.ParseEditedMessageTopicFromEventAction::class.java)
            .withLatestFrom(state) { action, currentState -> action to currentState }
            .flatMap { (action, state) ->
                val streamName = if (state.stream != null) {
                    state.stream.name
                } else {
                    state.topic!!.streamName
                }
                messagesRepository.updateMessageTopicInDatabase(action.messageId, action.newMessageTopic, streamName)
                    .subscribeOn(Schedulers.io())
                    .toSingleDefault(true)
                    .map<MessagesAction> { result -> MessagesAction.ParseEditedMessageFromEventSuccessAction }
                    .onErrorReturn { error -> MessagesAction.ParseEditedMessageFromEventFailureAction(error) }
                    .toObservable()
            }
    }
}