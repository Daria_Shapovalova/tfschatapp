package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class GetEventsFromQueueMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.GetEventsFromQueueAction::class.java)
            .flatMap { action ->
                messagesRepository.getEventsFromQueue(action.queueId, action.lastEventId)
                    .subscribeOn(Schedulers.io())
                    .map { result ->
                        if (result.result == EVENT_ERROR) {
                            MessagesAction.RegisterEventQueueAction
                        } else if (result.events!![0].type == EVENT_HEARTBEAT) {
                            MessagesAction.GetEventsFromQueueAction(action.streamName, action.topicName, action.queueId, result.events[0].id)
                        } else {
                            MessagesAction.ParseEventsAction(action.streamName, action.topicName, action.queueId, result.events)
                        }
                    }
                    .toObservable()
            }
            .onErrorReturn { error -> MessagesAction.GetEventsFromQueueFailureAction(error) }
    }

    companion object {
        private const val EVENT_ERROR = "error"
        private const val EVENT_HEARTBEAT = "heartbeat"
    }
}