package com.example.tfschatapp.presentation.mvi.middleware.streams

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.repository.StreamsRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class CreateNewStreamMiddleware(private val streamsRepository: StreamsRepository) : Middleware<StreamsAction, StreamsState> {
    override fun bind(actions: Observable<StreamsAction>, state: Observable<StreamsState>): Observable<StreamsAction> {
        return actions.ofType(StreamsAction.CreateNewStreamAction::class.java)
            .flatMap { action ->
                streamsRepository.createStream(action.streamName)
                    .toSingleDefault(true)
                    .subscribeOn(Schedulers.io())
                    .map<StreamsAction> { result -> StreamsAction.GetStreamIdAction(action.streamName) }
                    .onErrorReturn { error -> StreamsAction.CreateNewStreamFailureAction(error) }
                    .toObservable()
            }
    }
}