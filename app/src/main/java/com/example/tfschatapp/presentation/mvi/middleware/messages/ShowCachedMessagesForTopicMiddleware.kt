package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class ShowCachedMessagesForTopicMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.ShowCachedMessagesForTopicAction::class.java)
            .flatMap { action ->
                messagesRepository.getAllMessagesInTopicFromDatabase(action.topic.uid)
                    .subscribeOn(Schedulers.io())
                    .map<MessagesAction> { result ->
                        if (result != null) MessagesAction.ShowCachedMessagesSuccessAction(result)
                        else MessagesAction.ShowCachedMessagesSuccessAction(emptyList())
                    }
                    .onErrorReturn { error -> MessagesAction.ShowCachedMessagesFailureAction(error) }
                    .toObservable()
            }
    }
}