package com.example.tfschatapp.presentation.mvi.actions

import com.example.tfschatapp.presentation.modelui.UserUi

sealed class UsersAction {
    object LoadUsersAction : UsersAction()
    data class LoadUsersSuccessAction(val users: List<UserUi>) : UsersAction()
    data class LoadUsersFailureAction(val error: Throwable) : UsersAction()
    data class FilterUsersByQueryAction(val searchQuery: String) : UsersAction()
}