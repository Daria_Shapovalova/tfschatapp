package com.example.tfschatapp.presentation.mvi.middleware.users

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.ProfileAction
import com.example.tfschatapp.presentation.mvi.states.ProfileState
import com.example.tfschatapp.repository.UsersRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class ProfileCurrentUserMiddleware(private val usersRepository: UsersRepository) : Middleware<ProfileAction, ProfileState> {
    override fun bind(actions: Observable<ProfileAction>, state: Observable<ProfileState>): Observable<ProfileAction> {
        return actions.ofType(ProfileAction.LoadCurrentUserProfileAction::class.java)
            .flatMap { action ->
                usersRepository.getContactById(usersRepository.currentUserId)!!
                    .subscribeOn(Schedulers.io())
                    .map<ProfileAction> { result -> ProfileAction.LoadProfileSuccessAction(result) }
                    .onErrorReturn { error -> ProfileAction.LoadProfileFailureAction(error) }
            }
    }
}