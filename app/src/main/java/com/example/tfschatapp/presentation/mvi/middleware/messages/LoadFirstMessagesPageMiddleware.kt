package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class LoadFirstMessagesPageMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.LoadFirstMessagesPageAction::class.java)
            .withLatestFrom(state) { action, currentState -> action to currentState }
            .flatMap { (action, state) ->
                if (state.topic != null) {
                    loadMessagesInTopicFromNetwork(state.topic)
                } else {
                    loadMessagesInStreamFromNetwork(state.stream!!)
                }
            }
    }

    private fun loadMessagesInTopicFromNetwork(topic: TopicUi): Observable<MessagesAction> {
        return messagesRepository.loadMessagesInTopicFromNetwork(topic, LATEST_MESSAGE_ID_DEFAULT_VALUE)
            .map<MessagesAction> { result ->
                MessagesAction.LoadFirstMessagesPageSuccessAction(
                    result.first,
                    result.second.map { message -> message.toMessageUi(messagesRepository.currentUserId!!) },
                    result.second
                )
            }
            .subscribeOn(Schedulers.io())
            .toObservable()
            .onErrorReturn { error -> MessagesAction.LoadFirstMessagesPageFailureAction(error) }
    }

    private fun loadMessagesInStreamFromNetwork(stream: StreamUi): Observable<MessagesAction> {
        return messagesRepository.loadMessagesInStreamFromNetwork(stream, LATEST_MESSAGE_ID_DEFAULT_VALUE)
            .map<MessagesAction> { result ->
                MessagesAction.LoadFirstMessagesPageSuccessAction(
                    result.first,
                    result.second.map { message -> message.toMessageUi(messagesRepository.currentUserId!!) },
                    result.second
                )
            }
            .subscribeOn(Schedulers.io())
            .toObservable()
            .onErrorReturn { error -> MessagesAction.LoadFirstMessagesPageFailureAction(error) }
    }

    companion object {
        private const val LATEST_MESSAGE_ID_DEFAULT_VALUE = 10000000000000000
    }
}