package com.example.tfschatapp.presentation.fragments

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.tfschatapp.R
import com.example.tfschatapp.databinding.FragmentChannelsBinding
import com.example.tfschatapp.presentation.activity.ActivityWithBottomNavigation
import com.example.tfschatapp.presentation.activity.MainActivity
import com.example.tfschatapp.presentation.adapters.ViewPagerAdapter
import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.MviViewWithUiEffects
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.presentation.mvi.uieffects.MessagesUiEffect
import com.example.tfschatapp.presentation.mvi.uieffects.StreamsUiEffect
import com.example.tfschatapp.presentation.viewmodels.StreamsViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.jakewharton.rxrelay3.PublishRelay
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.ObservableOnSubscribe
import java.util.*
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class StreamsMainFragment : Fragment(), MviViewWithUiEffects<StreamsAction, StreamsState, StreamsUiEffect> {
    override val actions: PublishRelay<StreamsAction> = PublishRelay.create()
    val viewModel: StreamsViewModel by viewModels()

    private var _binding: FragmentChannelsBinding? = null
    private val binding get() = _binding!!

    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        _binding = FragmentChannelsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpStatusBar()
        setUpActionBar()
        showBottomNavigation()

        setUpTabLayout()
        setUpCreateNewStreamButton()

        viewModel.bind(this)
        loadStreamsDataFromNetwork()
        actions.accept(StreamsAction.LoadStreamsFromCacheAction)

        setUpRetryButton()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        setUpSearchView(menu, inflater)
        setUpObservableForSearchView()
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setUpStatusBar() {
        activity?.window?.statusBarColor = ContextCompat.getColor(requireContext(), R.color.dark_gray_navigation)
    }

    private fun setUpActionBar() {
        (activity as? AppCompatActivity)?.supportActionBar?.setBackgroundDrawable(
            ColorDrawable(ContextCompat.getColor(requireContext(), R.color.dark_gray_navigation))
        )
    }

    private fun showBottomNavigation() {
        (activity as ActivityWithBottomNavigation).showBottomNavigation()
    }

    private fun setUpTabLayout() {
        val fragmentManager = childFragmentManager
        val fragmentAdapter = ViewPagerAdapter(fragmentManager, lifecycle)
        binding.viewPager.adapter = fragmentAdapter

        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = when (position) {
                SUBSCRIBED_STREAMS_PAGE_POSITION -> getString(R.string.subscribed_channels_header)
                else -> getString(R.string.all_channels_header)
            }
        }.attach()
    }

    private fun setUpCreateNewStreamButton() {
        binding.createNewStreamButton.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            val inflater = requireActivity().layoutInflater
            val dialogLayout = inflater.inflate(R.layout.dialog_enter_stream_name, null)
            val streamNameEditText = dialogLayout.findViewById<EditText>(R.id.streamNameEditText)

            with(builder) {
                setTitle(getString(R.string.create_stream_dialog_header))
                setPositiveButton(getString(R.string.ok_button)) { dialog, which ->
                    val newStreamName = streamNameEditText.text.toString()
                    actions.accept(StreamsAction.CreateNewStreamAction(newStreamName))
                }
                setNegativeButton(getString(R.string.cancel_button)) { dialog, which -> }
                setView(dialogLayout)
                show()
            }
        }
    }

    private fun setUpSearchView(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search, menu)
        searchView = menu.findItem(R.id.search_field)?.actionView as SearchView
        searchView.queryHint = getString(R.string.channels_search_query_hint)
    }

    private fun setUpObservableForSearchView() {
        Observable.create(ObservableOnSubscribe<String> { subscriber ->
            setOnQueryTextListener(subscriber)
        })
            .map { text -> text.toLowerCase(Locale.ROOT).trim() }
            .debounce(SEARCH_TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { text ->
                actions.accept(StreamsAction.FilterStreamsByQueryAction(text))
            }
    }

    private fun setOnQueryTextListener(subscriber: ObservableEmitter<String>) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                subscriber.onNext(newText!!)
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                subscriber.onNext(query!!)
                return false
            }
        })
    }

    private fun loadStreamsDataFromNetwork() {
        actions.accept(StreamsAction.LoadStreamsFromNetworkAction)
    }

    private fun setUpRetryButton() {
        binding.errorNetworkLayout.networkErrorRetryButton.setOnClickListener {
            loadStreamsDataFromNetwork()
            (activity as MainActivity).loadUsersData()
        }
    }

    override fun render(state: StreamsState) {
        if (state.allStreams.isEmpty()) {
            binding.errorNetworkLayout.root.visibility = View.VISIBLE
            binding.viewPagerLayout.visibility = View.GONE
        } else {
            binding.errorNetworkLayout.root.visibility = View.GONE
            binding.viewPagerLayout.visibility = View.VISIBLE
        }
    }

    override fun handleUiEffect(uiEffect: StreamsUiEffect?) {
        when (uiEffect) {
            is StreamsUiEffect.CreateStreamSuccess -> navigateToSelectedStream(uiEffect.stream)
            is StreamsUiEffect.CreateStreamError -> showSnackBar(R.string.error_creating_stream)
            is StreamsUiEffect.GetStreamIdError -> showSnackBar(R.string.error_getting_stream_id)
            else -> {
            }
        }
    }

    private fun navigateToSelectedStream(selectedStream: StreamUi?) {
        if (selectedStream != null) {
            this.findNavController().navigate(StreamsMainFragmentDirections.actionShowStreamDiscussion(selectedStream))
        }
    }

    private fun showSnackBar(stringResource: Int) {
        Snackbar.make(requireView(), stringResource, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        viewModel.unbind()
        super.onDestroyView()
    }

    companion object {
        private const val SUBSCRIBED_STREAMS_PAGE_POSITION = 0
        private const val SEARCH_TIMEOUT_MILLISECONDS = 250L
    }
}