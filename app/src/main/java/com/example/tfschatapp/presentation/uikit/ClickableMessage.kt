package com.example.tfschatapp.presentation.uikit

import com.example.tfschatapp.presentation.modelui.MessageUi

interface ClickableMessage {
    var message: MessageUi?
}