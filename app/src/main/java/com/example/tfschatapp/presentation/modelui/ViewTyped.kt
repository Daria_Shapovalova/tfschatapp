package com.example.tfschatapp.presentation.modelui

interface ViewTyped {
    val viewType: Int
        get() = error("Provide viewType $this")

    val uid: String
        get() = error("Provide uid for viewType $this")

    override fun equals(other: Any?): Boolean
}