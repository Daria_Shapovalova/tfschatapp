package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import io.reactivex.rxjava3.core.Observable

class ParseEventsMiddleware : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.ParseEventsAction::class.java)
            .withLatestFrom(state) { action, currentState -> action to currentState }
            .flatMap { (action, state) ->
                val parseEventActions = mutableListOf<MessagesAction>()
                action.events.forEach {
                    when (it.type) {
                        EVENT_MESSAGE -> parseEventActions.add(MessagesAction.ParseMessageFromEventAction(it, action.streamName, action.topicName))
                        EVENT_REACTION -> parseEventActions.add(MessagesAction.ParseReactionFromEventAction(it))
                        EVENT_DELETE_MESSAGE -> parseEventActions.add(MessagesAction.ParseDeletedMessageFromEventAction(it.messageId.toString()))
                        EVENT_UPDATE_MESSAGE -> {
                            if (it.newMessageContent != null) {
                                parseEventActions.add(MessagesAction.ParseEditedMessageContentFromEventAction(it.messageId.toString(), it.newMessageContent))
                            }
                            if (it.newMessageTopic != null) {
                                parseEventActions.add(MessagesAction.ParseEditedMessageTopicFromEventAction(it.messageId.toString(), it.newMessageTopic))
                            }
                        }
                    }
                }
                parseEventActions.add(
                    MessagesAction.GetEventsFromQueueAction(
                        action.streamName,
                        action.topicName,
                        action.queueId,
                        action.events.maxOf { it.id })
                )
                if (state.topic != null) {
                    parseEventActions.add(MessagesAction.MarkAllMessagesInTopicAsReadAction(state.topic))
                    parseEventActions.add(MessagesAction.MarkCachedMessagesInTopicAsReadAction(state.topic))
                } else {
                    parseEventActions.add(MessagesAction.MarkAllMessagesInStreamAsReadAction(state.stream!!))
                    parseEventActions.add(MessagesAction.MarkCachedMessagesInStreamAsReadAction(state.stream))
                }

                Observable.fromIterable(parseEventActions)
            }
    }

    companion object {
        private const val EVENT_MESSAGE = "message"
        private const val EVENT_REACTION = "reaction"
        private const val EVENT_DELETE_MESSAGE = "delete_message"
        private const val EVENT_UPDATE_MESSAGE = "update_message"
    }
}