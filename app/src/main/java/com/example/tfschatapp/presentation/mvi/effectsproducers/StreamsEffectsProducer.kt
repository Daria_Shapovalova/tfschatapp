package com.example.tfschatapp.presentation.mvi.effectsproducers

import com.example.tfschatapp.presentation.mvi.EffectsProducer
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.uieffects.StreamsUiEffect

class StreamsEffectsProducer : EffectsProducer<StreamsAction, StreamsUiEffect> {
    override fun produce(action: StreamsAction): StreamsUiEffect {
        return when (action) {
            is StreamsAction.CreateNewStreamSuccessAction -> StreamsUiEffect.CreateStreamSuccess(action.stream)
            is StreamsAction.CreateNewStreamFailureAction -> StreamsUiEffect.CreateStreamError(action.error)
            is StreamsAction.GetStreamIdFailureAction -> StreamsUiEffect.GetStreamIdError(action.error)
            else -> { StreamsUiEffect.NoEffect }
        }
    }
}