package com.example.tfschatapp.presentation.modelui

import android.os.Parcelable
import com.example.tfschatapp.R
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Parcelize
@Serializable
data class EmojiUi(
    override val uid: String,
    val emoji: String,
    override val viewType: Int = R.layout.item_emoji
) : ViewTyped, Parcelable