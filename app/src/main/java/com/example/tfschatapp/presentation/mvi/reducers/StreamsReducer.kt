package com.example.tfschatapp.presentation.mvi.reducers

import com.example.tfschatapp.presentation.mvi.Reducer
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import java.util.*

class StreamsReducer : Reducer<StreamsState, StreamsAction> {
    override fun reduce(state: StreamsState, action: StreamsAction): StreamsState {
        return when (action) {
            is StreamsAction.LoadStreamsFromCacheAction -> state.copy(
                isLoading = true,
            )
            is StreamsAction.LoadStreamsFromCacheSuccessAction -> state.copy(
                allStreams = action.streams.first,
                filteredStreamsWithTopics = action.streams.first.flatMap { it.toStreamWithTopicsUi() },
                subscribedStreams = action.streams.second,
                subscribedFilteredStreamsWithTopics = action.streams.second.flatMap { it.toStreamWithTopicsUi() },
                isLoading = false
            )
            is StreamsAction.LoadStreamsFromCacheFailureAction -> state.copy(
                error = action.error,
                isLoading = false
            )
            is StreamsAction.FilterStreamsByQueryAction ->
                if (action.searchQuery != "") {
                    state.copy(
                        filteredStreamsWithTopics = state.allStreams.filter { it.name.toLowerCase(Locale.ROOT).contains(action.searchQuery) }
                            .flatMap { it.toStreamWithTopicsUi() },
                        subscribedFilteredStreamsWithTopics = state.subscribedStreams.filter { it.name.toLowerCase(Locale.ROOT).contains(action.searchQuery) }
                            .flatMap { it.toStreamWithTopicsUi() }
                    )
                } else {
                    state.copy(
                        filteredStreamsWithTopics = state.allStreams.flatMap { it.toStreamWithTopicsUi() },
                        subscribedFilteredStreamsWithTopics = state.subscribedStreams.flatMap { it.toStreamWithTopicsUi() }
                    )
                }
            else -> state
        }
    }
}