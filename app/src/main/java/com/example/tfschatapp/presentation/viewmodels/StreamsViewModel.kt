package com.example.tfschatapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import com.example.tfschatapp.presentation.mvi.MviViewWithUiEffects
import com.example.tfschatapp.presentation.mvi.StoreWithUiEffects
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.effectsproducers.StreamsEffectsProducer
import com.example.tfschatapp.presentation.mvi.middleware.streams.*
import com.example.tfschatapp.presentation.mvi.reducers.StreamsReducer
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.presentation.mvi.uieffects.StreamsUiEffect
import com.example.tfschatapp.repository.StreamsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class StreamsViewModel @Inject constructor(streamsRepository: StreamsRepository) : ViewModel() {
    private val store = StoreWithUiEffects(
        StreamsReducer(), listOf(
            LoadStreamsFromNetworkMiddleware(streamsRepository),
            LoadStreamsFromNetworkSuccessMiddleware(),
            CacheStreamsMiddleware(streamsRepository),
            LoadSubscribedStreamsIdsMiddleware(streamsRepository),
            MarkStreamsAsSubscribedMiddleware(streamsRepository),
            LoadTopicsFromNetworkMiddleware(streamsRepository),
            CacheTopicsMiddleware(streamsRepository),
            LoadUnreadMessageCountsMiddleware(streamsRepository),
            CacheUnreadMessageCountsMiddleware(streamsRepository),
            LoadStreamsFromCacheMiddleware(streamsRepository),
            OpenStreamMiddleware(streamsRepository),
            CreateNewStreamMiddleware(streamsRepository),
            GetStreamIdMiddleware(streamsRepository)
        ),
        StreamsState(),
        StreamsEffectsProducer()
    )

    private val wiring = store.wire()
    private var viewBinding: Disposable? = null

    fun bind(view: MviViewWithUiEffects<StreamsAction, StreamsState, StreamsUiEffect>) {
        viewBinding = store.bind(view)
    }

    fun unbind() {
        viewBinding?.dispose()
    }

    override fun onCleared() {
        wiring.dispose()
        super.onCleared()
    }
}