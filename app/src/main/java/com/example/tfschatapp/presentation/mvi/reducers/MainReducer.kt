package com.example.tfschatapp.presentation.mvi.reducers

import com.example.tfschatapp.presentation.mvi.Reducer
import com.example.tfschatapp.presentation.mvi.actions.MainAction
import com.example.tfschatapp.presentation.mvi.states.MainState

class MainReducer : Reducer<MainState, MainAction> {
    override fun reduce(state: MainState, action: MainAction): MainState {
        return MainState
    }
}