package com.example.tfschatapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.Store
import com.example.tfschatapp.presentation.mvi.actions.MainAction
import com.example.tfschatapp.presentation.mvi.middleware.users.*
import com.example.tfschatapp.presentation.mvi.reducers.MainReducer
import com.example.tfschatapp.presentation.mvi.states.MainState
import com.example.tfschatapp.repository.SharedPreferencesDao
import com.example.tfschatapp.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(usersRepository: UsersRepository, sharedPreferencesDao: SharedPreferencesDao) : ViewModel() {
    private val store = Store(
        MainReducer(), listOf(
            LoadUsersFromNetworkMiddleware(usersRepository),
            CacheLoadedUsersMiddleware(usersRepository),
            GetCurrentUserFromNetworkMiddleware(usersRepository, sharedPreferencesDao),
            ReportUserPresenceMiddleware(usersRepository),
            CacheUsersPresenceMiddleware(usersRepository)
        ), MainState
    )

    private val wiring = store.wire()
    private var viewBinding: Disposable? = null

    fun bind(view: MviView<MainAction, MainState>) {
        viewBinding = store.bind(view)
    }

    fun unbind() {
        viewBinding?.dispose()
    }

    override fun onCleared() {
        wiring.dispose()
        super.onCleared()
    }
}