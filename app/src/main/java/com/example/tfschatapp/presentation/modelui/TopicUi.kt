package com.example.tfschatapp.presentation.modelui

import android.os.Parcelable
import com.example.tfschatapp.R
import kotlinx.parcelize.Parcelize

@Parcelize
data class TopicUi(
    override val uid: String,
    val name: String,
    val messagesCount: Int = 0,
    val color: Int,
    val streamId: String,
    val streamName: String,
    val streamSubscribed: Boolean,
    override val viewType: Int = R.layout.item_topic
) : ViewTyped, Parcelable