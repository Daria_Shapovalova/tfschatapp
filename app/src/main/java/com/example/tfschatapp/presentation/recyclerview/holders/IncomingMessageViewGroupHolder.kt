package com.example.tfschatapp.presentation.recyclerview.holders

import android.view.View
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.recyclerview.BaseViewHolder
import com.example.tfschatapp.presentation.uikit.IncomingMessageViewGroup

class IncomingMessageViewGroupHolder(
    view: View, messageLongClick: ((View) -> Boolean)? = null, private val reactionClick: ((View) -> Unit)? = null, addReactionClick: ((View) -> Unit)? = null,
    contactClick: ContactClickListener? = null
) : BaseViewHolder<MessageUi>(view) {
    private val messageHolder: IncomingMessageViewGroup = view.findViewById(R.id.message)

    init {
        messageHolder.isClickable = true
        messageHolder.messageLongClickListener = messageLongClick
        messageHolder.addReactionClickListener = addReactionClick
        messageHolder.contactClickListener = contactClick
    }

    override fun bind(item: MessageUi) {
        messageHolder.message = item
        messageHolder.reactionClickListener = reactionClick
    }
}