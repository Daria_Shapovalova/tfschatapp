package com.example.tfschatapp.presentation.mvi.middleware.streams

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.repository.StreamsRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class OpenStreamMiddleware(private val streamsRepository: StreamsRepository) : Middleware<StreamsAction, StreamsState> {
    override fun bind(actions: Observable<StreamsAction>, state: Observable<StreamsState>): Observable<StreamsAction> {
        return actions.ofType(StreamsAction.OpenOrCloseStreamAction::class.java)
            .flatMap { action ->
                val streamDb = action.stream.toStreamDb()
                streamDb.isOpened = !action.stream.isOpened
                streamsRepository.updateStream(streamDb)
                    .subscribeOn(Schedulers.io())
                    .toSingleDefault(true)
                    .map<StreamsAction> { result -> StreamsAction.OpenOrCloseStreamSuccessAction }
                    .onErrorReturn { error -> StreamsAction.OpenOrCloseStreamFailureAction(error) }
                    .toObservable()
            }
    }
}