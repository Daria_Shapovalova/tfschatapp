package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.StreamsRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class MarkCachedMessagesInStreamAsReadMiddleware(private val streamsRepository: StreamsRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.MarkCachedMessagesInStreamAsReadAction::class.java)
            .flatMap { action ->
                val topicsMap = mutableMapOf<String, Int>()
                action.stream.topics.forEach { topicsMap[it.uid] = 0 }
                streamsRepository.saveUnreadMessageCountsToDatabase(topicsMap)
                    .subscribeOn(Schedulers.io())
                    .toSingleDefault(true)
                    .map<MessagesAction> { result -> MessagesAction.MarkCachedMessagesAsReadSuccessAction }
                    .onErrorReturn { error -> MessagesAction.MarkCachedMessagesAsReadFailureAction(error) }
                    .toObservable()
            }
    }
}