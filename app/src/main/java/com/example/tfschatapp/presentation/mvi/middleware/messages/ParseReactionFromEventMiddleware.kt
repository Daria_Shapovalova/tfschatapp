package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.database.modeldb.ReactionDb
import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class ParseReactionFromEventMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.ParseReactionFromEventAction::class.java)
            .flatMap { action ->
                val emoji = String(Character.toChars(action.event.emojiCode!!.toInt(radix = 16)))
                val reaction = ReactionDb(
                    uid = "${action.event.emojiName} ${action.event.userId}",
                    emojiName = action.event.emojiName!!,
                    emoji = emoji,
                    messageId = action.event.messageId.toString(),
                    userId = action.event.userId.toString()
                )
                if (action.event.operation == EVENT_REACTION_ADD) {
                    messagesRepository.addReactionToDatabase(reaction)
                        .subscribeOn(Schedulers.io())
                        .toSingleDefault(true)
                        .map<MessagesAction> { result -> MessagesAction.ParseReactionFromEventSuccessAction }
                        .onErrorReturn { error -> MessagesAction.ParseReactionFromEventFailureAction(error) }
                        .toObservable()
                } else {
                    messagesRepository.removeReactionFromDatabase(reaction)
                        .subscribeOn(Schedulers.io())
                        .toSingleDefault(true)
                        .map<MessagesAction> { result -> MessagesAction.ParseReactionFromEventSuccessAction }
                        .onErrorReturn { error -> MessagesAction.ParseReactionFromEventFailureAction(error) }
                        .toObservable()
                }
            }
    }

    companion object {
        private const val EVENT_REACTION_ADD = "add"
    }
}