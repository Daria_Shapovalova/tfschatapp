package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class LoadNextMessagesPageMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.LoadNextMessagesPageAction::class.java)
            .withLatestFrom(state) { action, currentState -> action to currentState }
            .flatMap { (action, state) ->
                if (state.lastMessageFound) {
                    Observable.just(MessagesAction.ScrolledToLastMessageAction)
                } else {
                    if (state.topic != null) {
                        loadMessagesInTopicFromNetwork(state.topic, state.lastLoadedMessageId)
                    } else {
                        loadMessagesInStreamFromNetwork(state.stream!!, state.lastLoadedMessageId)
                    }
                }
            }
    }

    private fun loadMessagesInTopicFromNetwork(topic: TopicUi, lastLoadedMessageId: Long): Observable<MessagesAction> {
        return messagesRepository.loadMessagesInTopicFromNetwork(topic = topic, anchor = lastLoadedMessageId)
            .map<MessagesAction> { result ->
                MessagesAction.LoadNextMessagesPageSuccessAction(
                    result.first,
                    result.second.map { message -> message.toMessageUi(messagesRepository.currentUserId!!) },
                    result.second
                )
            }
            .subscribeOn(Schedulers.io())
            .onErrorReturn { error -> MessagesAction.LoadNextMessagesPageFailureAction(error) }
            .toObservable()
    }

    private fun loadMessagesInStreamFromNetwork(stream: StreamUi, lastLoadedMessageId: Long): Observable<MessagesAction> {
        return messagesRepository.loadMessagesInStreamFromNetwork(stream = stream, anchor = lastLoadedMessageId)
            .map<MessagesAction> { result ->
                MessagesAction.LoadNextMessagesPageSuccessAction(
                    result.first,
                    result.second.map { message -> message.toMessageUi(messagesRepository.currentUserId!!) },
                    result.second
                )
            }
            .subscribeOn(Schedulers.io())
            .onErrorReturn { error -> MessagesAction.LoadNextMessagesPageFailureAction(error) }
            .toObservable()
    }
}