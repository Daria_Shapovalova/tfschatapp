package com.example.tfschatapp.presentation.mvi.middleware.users

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MainAction
import com.example.tfschatapp.presentation.mvi.states.MainState
import com.example.tfschatapp.repository.UsersRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class LoadUsersFromNetworkMiddleware(private val usersRepository: UsersRepository) : Middleware<MainAction, MainState> {
    override fun bind(actions: Observable<MainAction>, state: Observable<MainState>): Observable<MainAction> {
        return actions.ofType(MainAction.LoadUsersFromNetworkAction::class.java)
            .flatMap { action ->
                usersRepository.getUsersFromNetwork()!!
                    .subscribeOn(Schedulers.io())
                    .map<MainAction> { result -> MainAction.CacheUsersAction(result) }
                    .onErrorReturn { error -> MainAction.LoadUsersFromNetworkFailureAction(error) }
                    .toObservable()
            }
    }
}