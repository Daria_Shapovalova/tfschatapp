package com.example.tfschatapp.presentation.uikit.flexbox

import android.widget.BaseAdapter

abstract class FlexBoxAdapter<T> : BaseAdapter() {
    var items: List<T> = listOf()

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): T {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}