package com.example.tfschatapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.Store
import com.example.tfschatapp.presentation.mvi.actions.ProfileAction
import com.example.tfschatapp.presentation.mvi.middleware.users.ProfileCurrentUserMiddleware
import com.example.tfschatapp.presentation.mvi.reducers.ProfileReducer
import com.example.tfschatapp.presentation.mvi.states.ProfileState
import com.example.tfschatapp.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class CurrentUserProfileViewModel @Inject constructor(usersRepository: UsersRepository) : ViewModel() {
    private val store = Store(ProfileReducer(), listOf(ProfileCurrentUserMiddleware(usersRepository)), ProfileState())

    private val wiring = store.wire()
    private var viewBinding: Disposable? = null

    fun bind(view: MviView<ProfileAction, ProfileState>) {
        viewBinding = store.bind(view)
    }

    fun unbind() {
        viewBinding?.dispose()
    }

    override fun onCleared() {
        wiring.dispose()
    }
}