package com.example.tfschatapp.presentation.mvi.middleware.users

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.UsersAction
import com.example.tfschatapp.presentation.mvi.states.UsersState
import com.example.tfschatapp.repository.UsersRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class LoadUsersFromCacheMiddleware(private val usersRepository: UsersRepository) : Middleware<UsersAction, UsersState> {
    override fun bind(actions: Observable<UsersAction>, state: Observable<UsersState>): Observable<UsersAction> {
        return actions.ofType(UsersAction.LoadUsersAction::class.java)
            .flatMap { action ->
                usersRepository.getUsersObservableFromDatabase()
                    .subscribeOn(Schedulers.io())
                    .map<UsersAction> { result -> UsersAction.LoadUsersSuccessAction(result) }
                    .onErrorReturn { error -> UsersAction.LoadUsersFailureAction(error) }
            }
    }
}