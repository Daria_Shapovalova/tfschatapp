package com.example.tfschatapp.presentation.uikit

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import androidx.core.view.*
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.extensions.layout

class IncomingMessageBoxViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : ViewGroup(context, attrs, defStyleAttr, defStyleRes) {
    var messageText: String? = null
        set(value) {
            if (field != value) {
                field = value
                messageTextView.text = messageText
                requestLayout()
            }
        }

    var name: String? = null
        set(value) {
            if (field != value) {
                field = value
                nameView.text = name
                requestLayout()
            }
        }

    private val nameView: TextView
    private val messageTextView: TextView

    private val nameRect = Rect()
    private val messageTextRect = Rect()

    init {
        LayoutInflater.from(context).inflate(R.layout.incoming_message_box_view_group, this, true)
        nameView = findViewById(R.id.name)
        messageTextView = findViewById(R.id.text)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val nameLayoutParams = nameView.layoutParams as MarginLayoutParams
        val textLayoutParams = messageTextView.layoutParams as MarginLayoutParams

        measureChildWithMargins(nameView, widthMeasureSpec, 0, heightMeasureSpec, 0)
        val nameHeight = nameView.measuredHeight + nameLayoutParams.topMargin + nameLayoutParams.bottomMargin
        val nameWidth = nameView.measuredWidth + nameLayoutParams.leftMargin + nameLayoutParams.rightMargin

        measureChildWithMargins(messageTextView, widthMeasureSpec, 0, heightMeasureSpec, nameHeight)
        val messageTextHeight = messageTextView.measuredHeight + textLayoutParams.topMargin + textLayoutParams.bottomMargin
        val messageTextWidth = messageTextView.measuredWidth + textLayoutParams.leftMargin + textLayoutParams.rightMargin

        setMeasuredDimension(
            resolveSize(maxOf(nameWidth, messageTextWidth), widthMeasureSpec),
            resolveSize(nameHeight + messageTextHeight, heightMeasureSpec)
        )
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        nameRect.left = nameView.marginStart
        nameRect.top = nameView.marginTop
        nameRect.right = nameRect.left + nameView.measuredWidth
        nameRect.bottom = nameRect.top + nameView.measuredHeight
        nameView.layout(nameRect)

        messageTextRect.left = messageTextView.marginStart
        messageTextRect.top = nameView.bottom + nameView.marginBottom + messageTextView.marginTop
        messageTextRect.right = messageTextRect.left + messageTextView.measuredWidth
        messageTextRect.bottom = messageTextRect.top + messageTextView.measuredHeight
        messageTextView.layout(messageTextRect)
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(MATCH_PARENT, WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?) = MarginLayoutParams(context, attrs)

    override fun generateLayoutParams(p: LayoutParams?): LayoutParams = MarginLayoutParams(p)
}