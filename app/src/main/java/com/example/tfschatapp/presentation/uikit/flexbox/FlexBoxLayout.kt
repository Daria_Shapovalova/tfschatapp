package com.example.tfschatapp.presentation.uikit.flexbox

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.*
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.extensions.dpToPx
import com.example.tfschatapp.presentation.extensions.layout

class FlexBoxLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0,
) : ViewGroup(context, attrs, defStyleAttr, defStyleRes) {
    private var childViews: MutableList<View> = mutableListOf()

    private val childViewRect = Rect()
    private val plusView = ImageView(context)
    private var addViewWidth = context.dpToPx(DEFAULT_ADD_VIEW_WIDTH)
    private var addViewHeight = context.dpToPx(DEFAULT_ADD_VIEW_HEIGHT)

    private var addIconSrc: Int = DEFAULT_ADD_ICON
        set(value) {
            if (field != value) {
                field = value
                requestLayout()
            }
        }

    lateinit var adapter: FlexBoxAdapter<Any>

    var items: List<Any> = listOf()
        set(value) {
            if (field != value) {
                field = value
                removeAllViews()
                childViews = mutableListOf()
                adapter.items = items
                for (i: Int in items.indices) {
                    val childView = adapter.getView(i, null, this)
                    childView.isClickable = true
                    childView.setOnClickListener(itemClickListener)
                    addView(childView)
                }
                plusView.setImageResource(addIconSrc)
                plusView.layoutParams = MarginLayoutParams(addViewWidth.toInt(), addViewHeight.toInt())
                addView(plusView)
                requestLayout()
            }
        }

    var addIconClickListener: ((View) -> Unit)? = null
        set(value) {
            if (field != value) {
                field = value
                plusView.setOnClickListener(addIconClickListener)
            }
        }

    var itemClickListener: ((View) -> Unit)? = null
        set(value) {
            if (field != value) {
                field = value
                children.forEach {
                    it.isClickable = true
                    it.setOnClickListener(value)
                }
                plusView.setOnClickListener(addIconClickListener)
            }
        }

    init {
        setWillNotDraw(true)
        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.FlexBoxLayout).apply {
                addIconSrc = getResourceId(R.styleable.FlexBoxLayout_fb_add_icon_src, addIconSrc)
                addViewWidth = getDimension(R.styleable.FlexBoxLayout_fb_add_view_width, addViewWidth)
                addViewHeight = getDimension(R.styleable.FlexBoxLayout_fb_add_view_height, addViewHeight)
                recycle()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var height = 0
        var currentWidth = 0
        var childHeight = 0
        var childWidth = 0
        var maxChildHeight = 0
        children.forEach { child ->
            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0)
            childHeight = child.marginTop + child.measuredHeight + child.marginBottom
            childWidth = child.marginLeft + child.measuredWidth + child.marginRight
            if (childHeight > maxChildHeight) {
                maxChildHeight = childHeight
            }
            if (currentWidth + childWidth > MeasureSpec.getSize(widthMeasureSpec)) {
                height += maxChildHeight
                currentWidth = childWidth
                maxChildHeight = childHeight
            } else {
                currentWidth += childWidth
            }
        }
        setMeasuredDimension(
            resolveSize(MeasureSpec.getSize(widthMeasureSpec), widthMeasureSpec),
            resolveSize(height + maxChildHeight, heightMeasureSpec)
        )
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val layoutWidth = this.measuredWidth
        var currentWidth = 0
        var currentHeight = 0
        var maxChildHeight = 0
        children.forEach { child ->
            if (currentWidth + child.marginLeft + child.measuredWidth + child.marginRight > layoutWidth) {
                currentWidth = 0
                currentHeight += maxChildHeight
                maxChildHeight = 0
            }
            childViewRect.left = currentWidth + child.marginLeft
            childViewRect.top = currentHeight + child.marginTop
            childViewRect.right = childViewRect.left + child.measuredWidth
            childViewRect.bottom = childViewRect.top + child.measuredHeight
            currentWidth += child.marginLeft + child.measuredWidth + child.marginRight
            if (child.marginTop + child.measuredHeight + child.marginBottom > maxChildHeight) {
                maxChildHeight = child.marginTop + child.measuredHeight + child.marginBottom
            }
            child.layout(childViewRect)
        }
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?) = MarginLayoutParams(context, attrs)

    override fun generateLayoutParams(p: LayoutParams?): LayoutParams = MarginLayoutParams(p)

    companion object {
        private const val DEFAULT_ADD_VIEW_WIDTH = 45
        private const val DEFAULT_ADD_VIEW_HEIGHT = 30
        private const val DEFAULT_ADD_ICON = R.drawable.ic_plus
    }
}