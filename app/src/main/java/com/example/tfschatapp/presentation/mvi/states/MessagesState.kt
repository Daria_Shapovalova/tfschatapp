package com.example.tfschatapp.presentation.mvi.states

import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi

data class MessagesState(
    val topic: TopicUi? = null,
    val stream: StreamUi? = null,
    val isLoading: Boolean = true,
    val lastLoadedMessageId: Long = 10000000000000000,
    val lastMessageFound: Boolean = false,
    val messages: List<MessageUi> = emptyList(),
    val error: Throwable? = null,
    val eventsQueueId: String? = null,
    val currentUserId: String? = null
)