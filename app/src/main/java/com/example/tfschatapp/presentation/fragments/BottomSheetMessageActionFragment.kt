package com.example.tfschatapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.example.tfschatapp.R
import com.example.tfschatapp.databinding.BottomsheetMessageActionFragmentBinding
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.modelui.UpdateMessageRequest
import com.example.tfschatapp.presentation.modelui.UpdateMessageRequestType
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheetMessageActionFragment : BottomSheetDialogFragment() {
    private var _binding: BottomsheetMessageActionFragmentBinding? = null
    val binding get() = _binding!!

    var clickedMessage: MessageUi? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottomsheetMessageActionFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAddReactionMenuItem()
        setUpCopyToClipboardMenuItem()
        setUpEditMessageMenuItem()
        setUpDeleteMessageMenuItem()
    }

    private fun setUpAddReactionMenuItem() {
        val addReactionView = binding.addReactionAction
        addReactionView.menuItemName.text = getString(R.string.add_reaction_menu_item)
        addReactionView.root.setOnClickListener {
            val result = UpdateMessageRequest(clickedMessage!!, UpdateMessageRequestType.ADD_REACTION)
            setFragmentResult(MESSAGE_ACTION_REQUEST_KEY, bundleOf(MESSAGE_ACTION_BUNDLE_KEY to result))
            dismiss()
        }
    }

    private fun setUpCopyToClipboardMenuItem() {
        val copyToClipboardView = binding.copyToClipboardAction
        copyToClipboardView.menuItemName.text = getString(R.string.copy_to_clipboard_menu_item)
        copyToClipboardView.root.setOnClickListener {
            val result = UpdateMessageRequest(clickedMessage!!, UpdateMessageRequestType.COPY_MESSAGE)
            setFragmentResult(MESSAGE_ACTION_REQUEST_KEY, bundleOf(MESSAGE_ACTION_BUNDLE_KEY to result))
            dismiss()
        }
    }

    private fun setUpEditMessageMenuItem() {
        val editMessageView = binding.editMessageAction
        if (clickedMessage!!.viewType == R.layout.outgoing_message_item) {
            editMessageView.menuItemName.text = getString(R.string.edit_message_menu_item)
            editMessageView.root.setOnClickListener {
                val result = UpdateMessageRequest(clickedMessage!!, UpdateMessageRequestType.EDIT_MESSAGE)
                setFragmentResult(MESSAGE_ACTION_REQUEST_KEY, bundleOf(MESSAGE_ACTION_BUNDLE_KEY to result))
                dismiss()
            }
        } else {
            editMessageView.root.visibility = View.GONE
        }
    }

    private fun setUpDeleteMessageMenuItem() {
        val deleteMessageView = binding.deleteMessageAction
        if (clickedMessage!!.viewType == R.layout.outgoing_message_item) {
            deleteMessageView.menuItemName.text = getString(R.string.delete_message_menu_item)
            deleteMessageView.root.setOnClickListener {
                val result = UpdateMessageRequest(clickedMessage!!, UpdateMessageRequestType.DELETE_MESSAGE)
                setFragmentResult(MESSAGE_ACTION_REQUEST_KEY, bundleOf(MESSAGE_ACTION_BUNDLE_KEY to result))
                dismiss()
            }
        } else {
            deleteMessageView.root.visibility = View.GONE
        }
    }

    companion object {
        private const val MESSAGE_ACTION_REQUEST_KEY = "messageActionRequestKey"
        private const val MESSAGE_ACTION_BUNDLE_KEY = "messageActionBundleKey"
    }
}