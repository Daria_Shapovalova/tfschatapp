package com.example.tfschatapp.presentation.mvi.middleware.users

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MainAction
import com.example.tfschatapp.presentation.mvi.states.MainState
import com.example.tfschatapp.repository.UsersRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ReportUserPresenceMiddleware(private val usersRepository: UsersRepository) : Middleware<MainAction, MainState> {
    override fun bind(actions: Observable<MainAction>, state: Observable<MainState>): Observable<MainAction> {
        return actions.ofType(MainAction.ReportUserPresenceAction::class.java)
            .flatMap { action ->
                usersRepository.reportUserPresence()
                    .subscribeOn(Schedulers.io())
                    .repeatWhen { completed -> completed.delay(REPORT_USER_PRESENCE_SECONDS_INTERVAL, TimeUnit.SECONDS) }
                    .map<MainAction> { result -> MainAction.CacheUserPresenceAction(result) }
                    .onErrorReturn { error -> MainAction.ReportUserPresenceFailureAction(error) }
                    .toObservable()
            }
    }

    companion object {
        private const val REPORT_USER_PRESENCE_SECONDS_INTERVAL = 60L
    }
}