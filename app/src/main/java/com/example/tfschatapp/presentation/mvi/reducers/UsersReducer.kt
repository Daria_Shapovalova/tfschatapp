package com.example.tfschatapp.presentation.mvi.reducers

import com.example.tfschatapp.presentation.mvi.Reducer
import com.example.tfschatapp.presentation.mvi.actions.UsersAction
import com.example.tfschatapp.presentation.mvi.states.UsersState
import java.util.*

class UsersReducer : Reducer<UsersState, UsersAction> {
    override fun reduce(state: UsersState, action: UsersAction): UsersState {
        return when (action) {
            is UsersAction.LoadUsersAction -> state.copy(
                isLoading = true,
            )
            is UsersAction.LoadUsersSuccessAction -> state.copy(
                allUsers = action.users,
                filteredUsers = action.users,
                isLoading = false
            )
            is UsersAction.LoadUsersFailureAction -> state.copy(
                error = action.error,
                isLoading = false
            )
            is UsersAction.FilterUsersByQueryAction ->
                if (action.searchQuery != "") {
                    state.copy(
                        filteredUsers = state.allUsers.filter { it.name.toLowerCase(Locale.ROOT).contains(action.searchQuery) }
                    )
                } else {
                    state.copy(
                        filteredUsers = state.allUsers
                    )
                }
        }
    }
}