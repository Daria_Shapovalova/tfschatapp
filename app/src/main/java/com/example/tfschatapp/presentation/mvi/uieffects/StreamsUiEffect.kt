package com.example.tfschatapp.presentation.mvi.uieffects

import com.example.tfschatapp.presentation.modelui.StreamUi

sealed class StreamsUiEffect {
    class CreateStreamSuccess(val stream: StreamUi) : StreamsUiEffect()
    class CreateStreamError(val error: Throwable) : StreamsUiEffect()
    class GetStreamIdError(val error: Throwable) : StreamsUiEffect()
    object NoEffect : StreamsUiEffect()
}