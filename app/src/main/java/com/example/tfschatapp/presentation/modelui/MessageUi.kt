package com.example.tfschatapp.presentation.modelui

import android.os.Parcelable
import com.example.tfschatapp.database.modeldb.ReactionDb
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Parcelize
@Serializable
data class MessageUi(
    override val uid: String,
    val date: DateUi,
    val author: UserUi,
    var messageText: String,
    var reactions: List<ReactionUi> = listOf(),
    var topicId: String,
    var topicName: String,
    override val viewType: Int
) : ViewTyped, Parcelable {
    fun addNewReaction(reaction: ReactionDb, fromCurrentUser: Boolean) {
        val updatedReactions = reactions.toMutableList()
        updatedReactions.add(
            ReactionUi(
                uid = reaction.emojiName,
                messageId = reaction.messageId,
                emoji = EmojiUi(reaction.emojiName, reaction.emoji),
                count = 1,
                selected = fromCurrentUser
            )
        )
        reactions = updatedReactions
    }
}