package com.example.tfschatapp.presentation.recyclerview.holders

import android.view.View
import com.example.tfschatapp.databinding.ItemChannelBinding
import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.recyclerview.BaseViewHolder

class ChannelViewHolder(
    val view: View, private val streamNavigateClick: StreamClickListener? = null,
    private val streamShowTopicsClick: StreamClickListener? = null
) : BaseViewHolder<StreamUi>(view) {
    private var channelViewBinding: ItemChannelBinding = ItemChannelBinding.bind(view)

    override fun bind(item: StreamUi) {
        channelViewBinding.channel = item
        channelViewBinding.navigateClickListener = streamNavigateClick
        channelViewBinding.showTopicsClickListener = streamShowTopicsClick
    }
}

class StreamClickListener(val clickListener: (stream: StreamUi) -> Unit) {
    fun onClick(stream: StreamUi) = clickListener(stream)
}