package com.example.tfschatapp.presentation.recyclerview.holders

import android.view.View
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.recyclerview.BaseViewHolder
import com.example.tfschatapp.presentation.uikit.OutgoingMessageViewGroup

class OutgoingMessageViewGroupHolder(
    view: View, messageLongClick: ((View) -> Boolean)? = null, private val reactionClick: ((View) -> Unit)? = null, addReactionClick: ((View) -> Unit)? = null
) : BaseViewHolder<MessageUi>(view) {
    private val messageHolder: OutgoingMessageViewGroup = view.findViewById(R.id.message)

    init {
        messageHolder.isClickable = true
        messageHolder.messageLongClickListener = messageLongClick
        messageHolder.addReactionClickListener = addReactionClick
    }

    override fun bind(item: MessageUi) {
        messageHolder.message = item
        messageHolder.reactionClickListener = reactionClick
    }
}