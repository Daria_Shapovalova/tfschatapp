package com.example.tfschatapp.presentation.mvi.states

import com.example.tfschatapp.presentation.modelui.UserUi

data class ProfileState(
    val isLoading: Boolean = false,
    val userProfile: UserUi? = null,
    val error: Throwable? = null
)