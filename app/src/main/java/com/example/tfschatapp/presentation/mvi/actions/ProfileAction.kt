package com.example.tfschatapp.presentation.mvi.actions

import com.example.tfschatapp.presentation.modelui.UserUi

sealed class ProfileAction {
    data class LoadProfileAction(val userId: String) : ProfileAction()
    object LoadCurrentUserProfileAction : ProfileAction()
    data class LoadProfileSuccessAction(val profile: UserUi) : ProfileAction()
    data class LoadProfileFailureAction(val error: Throwable) : ProfileAction()
}