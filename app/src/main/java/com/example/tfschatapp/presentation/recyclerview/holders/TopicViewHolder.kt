package com.example.tfschatapp.presentation.recyclerview.holders

import android.view.View
import com.example.tfschatapp.databinding.ItemTopicBinding
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.recyclerview.BaseViewHolder

class TopicViewHolder(val view: View, private val topicClick: TopicClickListener? = null) : BaseViewHolder<TopicUi>(view) {
    private var topicViewBinding: ItemTopicBinding = ItemTopicBinding.bind(view)

    override fun bind(item: TopicUi) {
        topicViewBinding.topic = item
        topicViewBinding.clickListener = topicClick
    }
}

class TopicClickListener(val clickListener: (topic: TopicUi) -> Unit) {
    fun onClick(topic: TopicUi) = clickListener(topic)
}