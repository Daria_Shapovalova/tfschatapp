package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import io.reactivex.rxjava3.core.Observable

class RegisterEventsQueueSuccessMiddleware : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.RegisterEventQueueSuccessAction::class.java)
            .flatMap { action ->
                Observable.just(
                    MessagesAction.GetEventsFromQueueAction(
                        action.streamName,
                        action.topicName,
                        action.registerEventQueueResponse.queueId,
                        action.registerEventQueueResponse.lastEventId
                    )
                )
            }
    }
}