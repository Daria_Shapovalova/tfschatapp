package com.example.tfschatapp.presentation.mvi.states

import com.example.tfschatapp.presentation.modelui.UserUi

data class UsersState(
    val isLoading: Boolean = false,
    val allUsers: List<UserUi> = emptyList(),
    val filteredUsers: List<UserUi> = emptyList(),
    val error: Throwable? = null
)