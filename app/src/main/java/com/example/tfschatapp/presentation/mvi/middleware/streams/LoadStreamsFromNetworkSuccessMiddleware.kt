package com.example.tfschatapp.presentation.mvi.middleware.streams

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import io.reactivex.rxjava3.core.Observable

class LoadStreamsFromNetworkSuccessMiddleware : Middleware<StreamsAction, StreamsState> {
    override fun bind(actions: Observable<StreamsAction>, state: Observable<StreamsState>): Observable<StreamsAction> {
        return actions.ofType(StreamsAction.LoadStreamsFromNetworkSuccessAction::class.java)
            .flatMap { action ->
                val loadStreamsActions = mutableListOf<StreamsAction>()
                loadStreamsActions.add(StreamsAction.CacheStreamsAction(action.streams))
                loadStreamsActions.add(StreamsAction.LoadTopicsFromNetworkAction(action.streams))
                Observable.fromIterable(loadStreamsActions)
            }
    }
}