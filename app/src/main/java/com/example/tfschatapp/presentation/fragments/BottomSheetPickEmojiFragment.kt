package com.example.tfschatapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.RecyclerView
import com.example.tfschatapp.R
import com.example.tfschatapp.network.modelnetwork.AddOrRemoveReactionRequest
import com.example.tfschatapp.presentation.managers.EmojiManager
import com.example.tfschatapp.presentation.modelui.ViewTyped
import com.example.tfschatapp.presentation.recyclerview.AsyncAdapter
import com.example.tfschatapp.presentation.recyclerview.ChatHolderFactory
import com.example.tfschatapp.presentation.recyclerview.holders.EmojiClickListener
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheetPickEmojiFragment : BottomSheetDialogFragment() {
    var clickedMessageId: String? = null

    private lateinit var emojiClickListener: EmojiClickListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottomsheet_pick_emoji_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpClickListener()
        setUpRecyclerView()
    }

    private fun setUpClickListener() {
        emojiClickListener = EmojiClickListener { emoji ->
            val result = AddOrRemoveReactionRequest(clickedMessageId!!.toInt(), emoji.uid)
            setFragmentResult(PICK_EMOJI_REQUEST_KEY, bundleOf(PICK_EMOJI_BUNDLE_KEY to result))
            dismiss()
        }
    }

    private fun setUpRecyclerView() {
        val holderFactory = ChatHolderFactory(emojiClick = emojiClickListener)
        val adapter = AsyncAdapter<ViewTyped>(holderFactory)

        val recyclerView = view?.findViewById<RecyclerView>(R.id.pick_emoji_recycler_view)
        recyclerView?.adapter = adapter

        adapter.items = EmojiManager.emojis
    }

    companion object {
        private const val PICK_EMOJI_REQUEST_KEY = "pickEmojiRequestKey"
        private const val PICK_EMOJI_BUNDLE_KEY = "pickEmojiBundleKey"
    }
}