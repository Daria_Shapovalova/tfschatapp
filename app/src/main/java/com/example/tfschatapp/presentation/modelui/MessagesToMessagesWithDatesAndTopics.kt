package com.example.tfschatapp.presentation.modelui

import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.managers.ColorManager

class MessagesToMessagesWithDatesAndTopics {
    fun invoke(messages: List<MessageUi>, stream: StreamUi): List<ViewTyped> {
        var lastShownTopic = ""
        var lastShownDate = ""
        return messages.flatMap { message ->
            val items = mutableListOf<ViewTyped>()
            if (message.topicName != lastShownTopic) {
                lastShownTopic = message.topicName
                items.add(generateTopicItem(message, stream))
            }
            if (message.date.date != lastShownDate) {
                lastShownDate = (message.date.date)
                items.add(message.date)
            }
            items.add(message)
            items
        }
    }

    private fun generateTopicItem(message: MessageUi, stream: StreamUi): ViewTyped {
        var topic = stream.topics.find { it.name == message.topicName }?.copy(viewType = R.layout.item_topic_header)
        if (topic == null) {
            topic = TopicUi(
                uid = "${stream.name} ${message.topicName}",
                name = message.topicName,
                color = ColorManager.getRandomColor(),
                streamId = stream.uid,
                streamName = stream.name,
                streamSubscribed = stream.subscribed,
                viewType = R.layout.item_topic_header
            )
        }
        return topic
    }
}