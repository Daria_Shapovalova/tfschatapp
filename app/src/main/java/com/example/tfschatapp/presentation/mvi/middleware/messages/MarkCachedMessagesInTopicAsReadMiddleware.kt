package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.StreamsRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class MarkCachedMessagesInTopicAsReadMiddleware(private val streamsRepository: StreamsRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.MarkCachedMessagesInTopicAsReadAction::class.java)
            .flatMap { action ->
                streamsRepository.saveUnreadMessageCountsToDatabase(mapOf(Pair(action.topic.uid, 0)))
                    .subscribeOn(Schedulers.io())
                    .toSingleDefault(true)
                    .map<MessagesAction> { result -> MessagesAction.MarkCachedMessagesAsReadSuccessAction }
                    .onErrorReturn { error -> MessagesAction.MarkCachedMessagesAsReadFailureAction(error) }
                    .toObservable()
            }
    }
}