package com.example.tfschatapp.presentation.modelui

class MessagesToMessagesWithDates {
    fun invoke(messages: List<MessageUi>): List<ViewTyped> {
        var lastShownDate = ""
        return messages.flatMap { message ->
            if (message.date.date != lastShownDate) {
                lastShownDate = message.date.date
                listOf(message.date, message)
            } else listOf(message)
        }
    }
}