package com.example.tfschatapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.Store
import com.example.tfschatapp.presentation.mvi.actions.UsersAction
import com.example.tfschatapp.presentation.mvi.middleware.users.LoadUsersFromCacheMiddleware
import com.example.tfschatapp.presentation.mvi.reducers.UsersReducer
import com.example.tfschatapp.presentation.mvi.states.UsersState
import com.example.tfschatapp.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(usersRepository: UsersRepository) : ViewModel() {
    private val store = Store(UsersReducer(), listOf(LoadUsersFromCacheMiddleware(usersRepository)), UsersState())

    private val wiring = store.wire()
    private var viewBinding: Disposable? = null

    fun bind(view: MviView<UsersAction, UsersState>) {
        viewBinding = store.bind(view)
    }

    fun unbind() {
        viewBinding?.dispose()
    }

    override fun onCleared() {
        wiring.dispose()
        super.onCleared()
    }
}