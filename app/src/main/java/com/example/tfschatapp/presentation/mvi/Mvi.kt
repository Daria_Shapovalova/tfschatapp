package com.example.tfschatapp.presentation.mvi

import com.jakewharton.rxrelay3.BehaviorRelay
import com.jakewharton.rxrelay3.PublishRelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

interface MviView<A, S> {
    val actions: Observable<A>
    fun render(state: S)
}

interface MviViewWithUiEffects<A, S, E> : MviView<A, S> {
    fun handleUiEffect(uiEffect: E?)
}

interface Reducer<S, A> {
    fun reduce(state: S, action: A): S
}

interface EffectsProducer<A, E> {
    fun produce(action: A): E?
}

interface Middleware<A, S> {
    fun bind(actions: Observable<A>, state: Observable<S>): Observable<A>
}

open class Store<A, S>(
    private val reducer: Reducer<S, A>,
    private val middlewares: List<Middleware<A, S>>,
    private val initialState: S
) {
    private val state = BehaviorRelay.createDefault(initialState)
    private val actions = PublishRelay.create<A>()

    open fun wire(): Disposable {
        val disposable = CompositeDisposable()

        disposable.add(actions
            .withLatestFrom(state) { action, state ->
                reducer.reduce(state, action)
            }
            .distinctUntilChanged()
            .subscribe(state::accept)
        )

        disposable.add(Observable.merge<A>(
            middlewares.map { it.bind(actions, state) }
        ).subscribe(actions::accept)
        )

        return disposable
    }

    open fun bind(view: MviView<A, S>): Disposable {
        val disposable = CompositeDisposable()
        disposable.add(state.observeOn(AndroidSchedulers.mainThread()).subscribe(view::render))
        disposable.add(view.actions.subscribe(actions::accept))
        return disposable
    }
}

class StoreWithUiEffects<A, S, E>(
    private val reducer: Reducer<S, A>,
    private val middlewares: List<Middleware<A, S>>,
    private val initialState: S,
    private val effectsProducer: EffectsProducer<A, E>
) {
    private val state = BehaviorRelay.createDefault(initialState)
    private val actions = PublishRelay.create<A>()
    private val uiEffects = PublishRelay.create<E>()

    fun wire(): Disposable {
        val disposable = CompositeDisposable()

        disposable.add(actions
            .withLatestFrom(state) { action, state ->
                reducer.reduce(state, action)
            }
            .distinctUntilChanged()
            .subscribe(state::accept)
        )

        disposable.add(
            actions.map { action ->
                effectsProducer.produce(action)
            }
                .distinctUntilChanged()
                .subscribe(uiEffects::accept)
        )

        disposable.add(Observable.merge<A>(
            middlewares.map { it.bind(actions, state) }
        ).subscribe(actions::accept)
        )

        return disposable
    }

    fun bind(view: MviViewWithUiEffects<A, S, E>): Disposable {
        val disposable = CompositeDisposable()
        disposable.add(state.observeOn(AndroidSchedulers.mainThread()).subscribe(view::render))
        disposable.add(uiEffects.observeOn(AndroidSchedulers.mainThread()).subscribe(view::handleUiEffect))
        disposable.add(view.actions.subscribe(actions::accept))
        return disposable
    }
}