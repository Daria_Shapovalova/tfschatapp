package com.example.tfschatapp.presentation.activity

interface ActivityWithBottomNavigation {
    fun hideBottomNavigation()
    fun showBottomNavigation()
}