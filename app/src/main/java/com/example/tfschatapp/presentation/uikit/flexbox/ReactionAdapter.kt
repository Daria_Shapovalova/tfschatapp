package com.example.tfschatapp.presentation.uikit.flexbox

import android.view.View
import android.view.ViewGroup
import com.example.tfschatapp.presentation.extensions.dpToPx
import com.example.tfschatapp.presentation.modelui.ReactionUi
import com.example.tfschatapp.presentation.uikit.ReactionView

class ReactionAdapter : FlexBoxAdapter<ReactionUi>() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val reactionView = ReactionView(parent.context)
        reactionView.id = position
        reactionView.reaction = items[position]
        val layoutParams = ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, parent.context.dpToPx(DEFAULT_VIEW_HEIGHT).toInt())
        layoutParams.rightMargin = parent.context.dpToPx(DEFAULT_MARGIN_RIGHT).toInt()
        layoutParams.bottomMargin = parent.context.dpToPx(DEFAULT_MARGIN_BOTTOM).toInt()
        reactionView.layoutParams = layoutParams
        reactionView.isClickable = true
        return reactionView
    }

    companion object {
        private const val DEFAULT_MARGIN_RIGHT = 10
        private const val DEFAULT_MARGIN_BOTTOM = 7
        private const val DEFAULT_VIEW_HEIGHT = 30
    }
}