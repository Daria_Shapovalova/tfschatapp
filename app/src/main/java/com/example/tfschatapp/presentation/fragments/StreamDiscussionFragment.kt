package com.example.tfschatapp.presentation.fragments

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import com.example.tfschatapp.R
import com.example.tfschatapp.databinding.FragmentStreamDiscussionBinding
import com.example.tfschatapp.network.modelnetwork.AddOrRemoveReactionRequest
import com.example.tfschatapp.network.modelnetwork.EditMessageRequest
import com.example.tfschatapp.network.modelnetwork.SendMessageRequest
import com.example.tfschatapp.presentation.activity.ActivityWithBottomNavigation
import com.example.tfschatapp.presentation.modelui.*
import com.example.tfschatapp.presentation.mvi.MviViewWithUiEffects
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.presentation.mvi.uieffects.MessagesUiEffect
import com.example.tfschatapp.presentation.recyclerview.AsyncAdapter
import com.example.tfschatapp.presentation.recyclerview.ChatHolderFactory
import com.example.tfschatapp.presentation.recyclerview.holders.ContactClickListener
import com.example.tfschatapp.presentation.recyclerview.holders.TopicClickListener
import com.example.tfschatapp.presentation.uikit.ClickableMessage
import com.example.tfschatapp.presentation.uikit.ReactionView
import com.example.tfschatapp.presentation.viewmodels.StreamDiscussionViewModel
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxrelay3.PublishRelay
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StreamDiscussionFragment : Fragment(), MviViewWithUiEffects<MessagesAction, MessagesState, MessagesUiEffect> {
    override val actions: PublishRelay<MessagesAction> = PublishRelay.create()
    private val viewModel: StreamDiscussionViewModel by viewModels()

    private var isLoading = false

    private var _binding: FragmentStreamDiscussionBinding? = null
    val binding get() = _binding!!

    private lateinit var stream: StreamUi

    private lateinit var navigateToTopicClickListener: TopicClickListener
    private lateinit var contactClickListener: ContactClickListener
    private lateinit var messageLongClickListener: (View) -> Boolean
    private lateinit var addReactionClickListener: (View) -> Unit
    private lateinit var reactionClickListener: (View) -> Unit

    private lateinit var holderFactory: ChatHolderFactory
    private lateinit var adapter: AsyncAdapter<ViewTyped>

    private var sendButtonActive: Boolean = false

    private val bottomSheetPickEmojiFragment = BottomSheetPickEmojiFragment()
    private val bottomSheetMessageActionFragment = BottomSheetMessageActionFragment()

    private var newMessageText: String = ""
    private var newMessageTopic: String = ""
    private var newMessageTopicChanged: Boolean = false

    private var messageInputMode = MessageInputMode.NEW_MESSAGE
    private var editMessageId: String? = null

    private var scrollToBottom: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStreamDiscussionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        stream = StreamDiscussionFragmentArgs.fromBundle(requireArguments()).selectedStream

        setUpStatusBar()
        setUpActionBar()
        hideBottomNavigation()

        setUpClickListeners()
        setUpRecyclerView()
        setUpOnScrollListener()

        setUpPickEmojiResultListener()
        setUpMessageActionResultListener()

        setUpMessageTextField()
        setUpMessageTopicField()
        setUpSendButton()

        viewModel.bind(this)
        actions.accept(MessagesAction.ShowCachedMessagesForStreamAction(stream))

        loadMessagesFromNetwork()
        setUpRetryButton()
    }

    private fun loadMessagesFromNetwork() {
        actions.accept(MessagesAction.LoadFirstMessagesPageAction)
        actions.accept(MessagesAction.RegisterEventQueueAction)
        actions.accept(MessagesAction.MarkAllMessagesInStreamAsReadAction(stream))
        actions.accept(MessagesAction.MarkCachedMessagesInStreamAsReadAction(stream))
    }

    private fun setUpRetryButton() {
        binding.errorNetworkLayout.networkErrorRetryButton.setOnClickListener {
            loadMessagesFromNetwork()
            binding.errorNetworkLayout.root.visibility = View.GONE
            binding.shimmerPlaceholderLayout.visibility = View.VISIBLE
        }
    }

    private fun setUpStatusBar() {
        activity?.window?.statusBarColor = ContextCompat.getColor(requireContext(), R.color.dark_gray_navigation)
    }

    private fun setUpActionBar() {
        (activity as? AppCompatActivity)?.supportActionBar?.apply {
            title = getString(R.string.stream_name, stream.name)
            setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(requireContext(), R.color.dark_gray_navigation)))
        }
    }

    private fun hideBottomNavigation() {
        (activity as ActivityWithBottomNavigation).hideBottomNavigation()
    }

    private fun setUpClickListeners() {
        navigateToTopicClickListener = TopicClickListener { topic -> navigateToSelectedTopic(topic) }

        contactClickListener = ContactClickListener { user ->
            if (user.name != NOTIFICATION_BOT_NAME) {
                actions.accept(MessagesAction.NavigateToUserProfile(user))
            }
        }

        messageLongClickListener = {
            bottomSheetMessageActionFragment.clickedMessage = (it.parent as ClickableMessage).message
            bottomSheetMessageActionFragment.show(childFragmentManager, BOTTOM_SHEET_MESSAGE_ACTION_DIALOG_TAG)
            true
        }

        addReactionClickListener = {
            bottomSheetPickEmojiFragment.clickedMessageId = (it.parent.parent as ClickableMessage).message?.uid
            bottomSheetPickEmojiFragment.show(childFragmentManager, BOTTOM_SHEET_PICK_EMOJI_DIALOG_TAG)
        }

        reactionClickListener = {
            val clickedMessage = (it.parent.parent as ClickableMessage).message!!
            val clickedMessageId = clickedMessage.uid.toInt()
            val emojiName = (it as ReactionView).reaction!!.emoji.uid
            if (clickedMessage.reactions.find { reaction -> reaction.emoji.uid == emojiName && reaction.selected } == null) {
                actions.accept(MessagesAction.AddReactionAction(AddOrRemoveReactionRequest(clickedMessageId, emojiName)))
            } else {
                actions.accept(MessagesAction.RemoveReactionAction(AddOrRemoveReactionRequest(clickedMessageId, emojiName)))
            }
        }
    }

    private fun setUpRecyclerView() {
        setUpRecyclerViewAdapter()
        binding.messagesRecyclerView.adapter = adapter
    }

    private fun setUpRecyclerViewAdapter() {
        holderFactory = ChatHolderFactory(
            messageLongClick = messageLongClickListener, reactionClick = reactionClickListener, addReactionClick = addReactionClickListener,
            contactClick = contactClickListener, topicClick = navigateToTopicClickListener
        )
        adapter = AsyncAdapter(holderFactory)
        adapter.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                if (scrollToBottom) scrollToBottom()
            }
        })
    }

    private fun setUpOnScrollListener() {
        binding.messagesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (linearLayoutManager.findFirstVisibleItemPosition() == 5) {
                    if (!isLoading) {
                        actions.accept(MessagesAction.LoadNextMessagesPageAction)
                    }
                }
            }
        })
    }

    private fun setUpPickEmojiResultListener() {
        childFragmentManager.setFragmentResultListener(PICK_EMOJI_REQUEST_KEY, this) { requestKey, bundle ->
            val result = bundle.getParcelable<AddOrRemoveReactionRequest>(PICK_EMOJI_BUNDLE_KEY)
            actions.accept(MessagesAction.AddReactionAction(result!!))
        }
    }

    private fun setUpMessageActionResultListener() {
        childFragmentManager.setFragmentResultListener(MESSAGE_ACTION_REQUEST_KEY, this) { requestKey, bundle ->
            val result = bundle.getParcelable<UpdateMessageRequest>(MESSAGE_ACTION_BUNDLE_KEY)
            when (result!!.updateMessageRequestType) {
                UpdateMessageRequestType.ADD_REACTION -> actions.accept(MessagesAction.SelectReactionAction(result.messageUi.uid))
                UpdateMessageRequestType.COPY_MESSAGE -> actions.accept(MessagesAction.CopyToClipboardAction(result.messageUi.messageText))
                UpdateMessageRequestType.EDIT_MESSAGE -> actions.accept(MessagesAction.OpenMessageEditorAction(result.messageUi))
                UpdateMessageRequestType.DELETE_MESSAGE -> actions.accept(MessagesAction.DeleteMessageAction(result.messageUi.uid))
            }
        }
    }

    private fun setUpMessageTextField() {
        binding.newMessageText.addTextChangedListener {
            newMessageText = binding.newMessageText.text.toString()
            sendButtonActive = newMessageText != ""
            if (sendButtonActive) {
                binding.sendButton.isEnabled = true
                when (messageInputMode) {
                    MessageInputMode.NEW_MESSAGE -> binding.sendButton.setImageResource(R.drawable.ic_send_message)
                    MessageInputMode.EDIT_MESSAGE -> binding.sendButton.setImageResource(R.drawable.ic_check)
                }
                binding.topicEditText.visibility = View.VISIBLE
                if (!newMessageTopicChanged) {
                    binding.topicEditText.setText(newMessageTopic)
                }
            } else {
                binding.sendButton.isEnabled = false
                binding.sendButton.setImageResource(R.drawable.ic_send_image)
                binding.topicEditText.visibility = View.GONE
            }
        }
    }

    private fun setUpMessageTopicField() {
        binding.topicEditText.addTextChangedListener {
            newMessageTopic = binding.topicEditText.text.toString()
            newMessageTopicChanged = true
        }
    }

    private fun setUpSendButton() {
        binding.sendButton.isEnabled = false
        binding.sendButton.setOnClickListener {
            when (messageInputMode) {
                MessageInputMode.NEW_MESSAGE ->
                    actions.accept(MessagesAction.SendMessageAction(SendMessageRequest(stream.name, newMessageTopic, newMessageText)))
                MessageInputMode.EDIT_MESSAGE -> {
                    actions.accept(MessagesAction.EditMessageAction(EditMessageRequest(editMessageId!!, newMessageTopic, newMessageText)))
                    editMessageId = null
                    messageInputMode = MessageInputMode.NEW_MESSAGE
                }
            }
            closeKeyboard()
        }
    }

    private fun hideShimmerPlaceholder() {
        binding.shimmerPlaceholderLayout.stopShimmer()
        binding.shimmerPlaceholderLayout.visibility = View.GONE
    }

    override fun render(state: MessagesState) {
        isLoading = state.isLoading
        if (!state.isLoading) {
            hideShimmerPlaceholder()
            if (state.messages.isEmpty() && state.error != null) {
                binding.errorNetworkLayout.root.visibility = View.VISIBLE
            } else {
                binding.errorNetworkLayout.root.visibility = View.GONE
                adapter.items = MessagesToMessagesWithDatesAndTopics().invoke(state.messages, stream)
                val linearLayoutManager = binding.messagesRecyclerView.layoutManager as LinearLayoutManager
                if (linearLayoutManager.findLastVisibleItemPosition() >= adapter.itemCount - 3) {
                    scrollToBottom = true
                }
                newMessageTopic = (adapter.items.findLast { it.viewType == R.layout.item_topic_header } as TopicUi).name
            }
        }
    }

    override fun handleUiEffect(uiEffect: MessagesUiEffect?) {
        when (uiEffect) {
            is MessagesUiEffect.LoadNextPageError -> showSnackBar(R.string.error_loading_messages)
            is MessagesUiEffect.SendMessageSuccess, MessagesUiEffect.EditMessageSuccess -> clearEditTextField()
            is MessagesUiEffect.SendMessageError -> showSnackBar(R.string.error_sending_message)
            is MessagesUiEffect.AddReactionError -> showSnackBar(R.string.error_adding_reaction)
            is MessagesUiEffect.RemoveReactionError -> showSnackBar(R.string.error_removing_reaction)
            is MessagesUiEffect.NavigateToUserProfile -> navigateToUserProfile(uiEffect.user)
            is MessagesUiEffect.ScrollToBottom -> scrollToBottom = true
            is MessagesUiEffect.OpenPickEmojiFragment -> openPickEmojiButtomSheetFragment(uiEffect.messageId)
            is MessagesUiEffect.CopyMessageToClipboard -> copyMessageToClipboard(uiEffect.messageText)
            is MessagesUiEffect.DeleteMessageError -> showSnackBar(R.string.error_deleting_message)
            is MessagesUiEffect.OpenMessageEditorAction -> openMessageEditor(uiEffect.message)
            is MessagesUiEffect.EditMessageError -> {
                clearEditTextField()
                showSnackBar(R.string.error_editing_message)
            }
            else -> {
            }
        }
    }

    private fun clearEditTextField() {
        binding.newMessageText.text.clear()
        newMessageTopicChanged = false
    }

    private fun navigateToUserProfile(user: UserUi) {
        this.findNavController().navigate(TopicDiscussionFragmentDirections.actionShowContactDetailsFromDiscussion(user))
    }

    private fun scrollToBottom() {
        binding.messagesRecyclerView.scrollToPosition(adapter.itemCount - 1)
        scrollToBottom = false
    }

    private fun openPickEmojiButtomSheetFragment(messageId: String) {
        bottomSheetPickEmojiFragment.clickedMessageId = messageId
        bottomSheetPickEmojiFragment.show(childFragmentManager, BOTTOM_SHEET_PICK_EMOJI_DIALOG_TAG)
    }

    private fun copyMessageToClipboard(messageText: String) {
        val clipboardManager = requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText(COPIED_MESSAGE_CLIP_LABEL, messageText)
        clipboardManager.setPrimaryClip(clipData)
        showSnackBar(R.string.copy_message_to_clipboard_success)
    }

    private fun openMessageEditor(message: MessageUi) {
        editMessageId = message.uid
        messageInputMode = MessageInputMode.EDIT_MESSAGE

        binding.topicEditText.visibility = View.VISIBLE
        binding.topicEditText.visibility = View.VISIBLE
        binding.topicEditText.setText(message.topicName)

        newMessageText = message.messageText.trimEnd()
        newMessageTopic = message.topicName

        binding.newMessageText.setText(newMessageText)
        binding.newMessageText.requestFocus(newMessageText.length)

        openKeyboard()
    }

    private fun showSnackBar(stringResource: Int?) {
        if (stringResource != null) {
            Snackbar.make(requireView(), stringResource, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun openKeyboard() {
        val view = requireActivity().currentFocus
        if (view != null) {
            val manager: InputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        }
    }

    private fun closeKeyboard() {
        val view = requireActivity().currentFocus
        if (view != null) {
            val manager: InputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun navigateToSelectedTopic(selectedTopic: TopicUi?) {
        if (selectedTopic != null) {
            this.findNavController().navigate(StreamDiscussionFragmentDirections.actionShowTopicDiscussion(selectedTopic))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.unbind()
    }

    companion object {
        private const val BOTTOM_SHEET_PICK_EMOJI_DIALOG_TAG = "BottomSheetPickEmojiDialog"
        private const val PICK_EMOJI_REQUEST_KEY = "pickEmojiRequestKey"
        private const val PICK_EMOJI_BUNDLE_KEY = "pickEmojiBundleKey"
        private const val BOTTOM_SHEET_MESSAGE_ACTION_DIALOG_TAG = "BottomSheetMessageActionDialog"
        private const val MESSAGE_ACTION_REQUEST_KEY = "messageActionRequestKey"
        private const val MESSAGE_ACTION_BUNDLE_KEY = "messageActionBundleKey"
        private const val COPIED_MESSAGE_CLIP_LABEL = "message"
        private const val NOTIFICATION_BOT_NAME = "Notification Bot"
    }

    enum class MessageInputMode {
        NEW_MESSAGE, EDIT_MESSAGE
    }
}