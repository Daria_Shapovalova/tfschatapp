package com.example.tfschatapp.presentation.mvi.uieffects

import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.modelui.UserUi

sealed class MessagesUiEffect {
    class LoadNextPageError(val error: Throwable) : MessagesUiEffect()
    object SendMessageSuccess : MessagesUiEffect()
    class SendMessageError(val error: Throwable) : MessagesUiEffect()
    class AddReactionError(val error: Throwable) : MessagesUiEffect()
    class RemoveReactionError(val error: Throwable) : MessagesUiEffect()
    class NavigateToUserProfile(val user: UserUi) : MessagesUiEffect()
    object ScrollToBottom : MessagesUiEffect()
    class OpenPickEmojiFragment(val messageId: String) : MessagesUiEffect()
    class CopyMessageToClipboard(val messageText: String) : MessagesUiEffect()
    class DeleteMessageError(val error: Throwable) : MessagesUiEffect()
    class OpenMessageEditorAction(val message: MessageUi) : MessagesUiEffect()
    object EditMessageSuccess : MessagesUiEffect()
    class EditMessageError(val error: Throwable) : MessagesUiEffect()
    object NoEffect : MessagesUiEffect()
}