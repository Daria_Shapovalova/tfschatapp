package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class ParseDeletedMessageFromEventMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.ParseDeletedMessageFromEventAction::class.java)
            .flatMap { action ->
                messagesRepository.removeMessageFromDatabaseById(action.messageId)
                    .subscribeOn(Schedulers.io())
                    .toSingleDefault(true)
                    .map<MessagesAction> { result -> MessagesAction.ParseDeletedMessageFromEventSuccessAction }
                    .onErrorReturn { error -> MessagesAction.ParseDeletedMessageFromEventFailureAction(error) }
                    .toObservable()
            }
    }
}