package com.example.tfschatapp.presentation.modelui

import android.os.Parcelable
import com.example.tfschatapp.R
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Parcelize
@Serializable
data class DateUi(
    override val uid: String = DATE_ID,
    val date: String,
    override val viewType: Int = R.layout.item_date
) : ViewTyped, Parcelable {
    companion object {
        private const val DATE_ID = "00"
    }
}