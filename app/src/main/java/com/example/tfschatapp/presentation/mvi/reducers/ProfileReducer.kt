package com.example.tfschatapp.presentation.mvi.reducers

import com.example.tfschatapp.presentation.mvi.Reducer
import com.example.tfschatapp.presentation.mvi.actions.ProfileAction
import com.example.tfschatapp.presentation.mvi.states.ProfileState

class ProfileReducer : Reducer<ProfileState, ProfileAction> {
    override fun reduce(state: ProfileState, action: ProfileAction): ProfileState {
        return when (action) {
            is ProfileAction.LoadProfileAction, ProfileAction.LoadCurrentUserProfileAction -> state.copy(
                isLoading = true,
            )
            is ProfileAction.LoadProfileSuccessAction -> state.copy(
                userProfile = action.profile,
                isLoading = false
            )
            is ProfileAction.LoadProfileFailureAction -> state.copy(
                error = action.error,
                isLoading = false
            )
        }
    }
}