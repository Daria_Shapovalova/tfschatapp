package com.example.tfschatapp.presentation.recyclerview.holders

import android.view.View
import com.example.tfschatapp.databinding.ItemEmojiBinding
import com.example.tfschatapp.presentation.modelui.EmojiUi
import com.example.tfschatapp.presentation.recyclerview.BaseViewHolder

class EmojiViewHolder(view: View, private val emojiClickListener: EmojiClickListener? = null) : BaseViewHolder<EmojiUi>(view) {
    private var emojiViewBinding: ItemEmojiBinding = ItemEmojiBinding.bind(view)

    override fun bind(item: EmojiUi) {
        emojiViewBinding.emoji = item
        emojiViewBinding.clickListener = emojiClickListener
    }
}

class EmojiClickListener(val clickListener: (emoji: EmojiUi) -> Unit) {
    fun onClick(emoji: EmojiUi) = clickListener(emoji)
}