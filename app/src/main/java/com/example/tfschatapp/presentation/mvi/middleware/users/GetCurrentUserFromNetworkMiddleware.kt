package com.example.tfschatapp.presentation.mvi.middleware.users

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MainAction
import com.example.tfschatapp.presentation.mvi.states.MainState
import com.example.tfschatapp.repository.SharedPreferencesDao
import com.example.tfschatapp.repository.UsersRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class GetCurrentUserFromNetworkMiddleware(private val usersRepository: UsersRepository, private val sharedPreferencesDao: SharedPreferencesDao) :
    Middleware<MainAction, MainState> {
    override fun bind(actions: Observable<MainAction>, state: Observable<MainState>): Observable<MainAction> {
        return actions.ofType(MainAction.GetCurrentUserAction::class.java)
            .flatMap { action ->
                usersRepository.getOwnUserIdFromNetwork()!!
                    .subscribeOn(Schedulers.io())
                    .map<MainAction> { result ->
                        sharedPreferencesDao.saveCurrentUserId(result)
                        MainAction.GetCurrentUserSuccessAction }
                    .onErrorReturn { error -> MainAction.GetCurrentUserFailureAction(error) }
                    .toObservable()
            }
    }
}