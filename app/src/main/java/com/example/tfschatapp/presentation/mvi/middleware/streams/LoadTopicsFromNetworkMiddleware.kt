package com.example.tfschatapp.presentation.mvi.middleware.streams

import com.example.tfschatapp.database.modeldb.StreamDb
import com.example.tfschatapp.database.modeldb.TopicDb
import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.repository.StreamsRepository
import io.reactivex.rxjava3.core.Observable

class LoadTopicsFromNetworkMiddleware(private val streamsRepository: StreamsRepository) : Middleware<StreamsAction, StreamsState> {
    override fun bind(actions: Observable<StreamsAction>, state: Observable<StreamsState>): Observable<StreamsAction> {
        return actions.ofType(StreamsAction.LoadTopicsFromNetworkAction::class.java)
            .flatMap { action ->
                Observable.fromIterable(action.streams)
                    .flatMap { stream: StreamDb -> streamsRepository.getTopicsInStreamFromNetwork(stream.uid.toInt())!! }
                    .flatMapIterable { list: List<TopicDb> -> list }
                    .toList()
                    .map<StreamsAction> { StreamsAction.CacheTopicsAction(it) }
                    .onErrorReturn { error -> StreamsAction.LoadTopicsFromNetworkFailureAction(error) }
                    .toObservable()
            }
    }
}