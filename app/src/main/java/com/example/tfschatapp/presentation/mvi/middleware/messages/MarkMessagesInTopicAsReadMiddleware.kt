package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class MarkMessagesInTopicAsReadMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.MarkAllMessagesInTopicAsReadAction::class.java)
            .flatMap { action ->
                messagesRepository.markMessagesAsReadInTopic(action.topic)
                    .subscribeOn(Schedulers.io())
                    .toSingleDefault(true)
                    .map<MessagesAction> { result -> MessagesAction.MarkAllMessagesAsReadSuccessAction }
                    .onErrorReturn { error -> MessagesAction.MarkAllMessagesAsReadFailureAction(error) }
                    .toObservable()
            }
    }
}