package com.example.tfschatapp.presentation.uikit

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.extensions.dpToPx
import com.example.tfschatapp.presentation.extensions.spToPx
import com.example.tfschatapp.presentation.modelui.ReactionUi

class ReactionView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {
    @ColorInt
    private var viewBackgroundColor: Int = ContextCompat.getColor(context, DEFAULT_BACKGROUND_COLOR)
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    @ColorInt
    private var viewSelectedBackgroundColor: Int =
        ContextCompat.getColor(context, SELECTED_BACKGROUND_COLOR)
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    private var cornerRadius = context.dpToPx(DEFAULT_CORNER_RADIUS)
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    private var paddingHorizontal = context.dpToPx(DEFAULT_PADDING_HORIZONTAL)
        set(value) {
            if (field != value) {
                field = value
                requestLayout()
            }
        }

    private var paddingVertical = context.dpToPx(DEFAULT_PADDING_VERTICAL)
        set(value) {
            if (field != value) {
                field = value
                requestLayout()
            }
        }

    @ColorInt
    private var textColor: Int = ContextCompat.getColor(context, DEFAULT_TEXT_COLOR)
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    @ColorInt
    private var textSelectedColor: Int = ContextCompat.getColor(context, SELECTED_TEXT_COLOR)
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    private var fontSize: Float = context.spToPx(DEFAULT_FONT_SIZE)
        set(value) {
            if (field != value) {
                field = value
                requestLayout()
            }
        }

    var reaction: ReactionUi? = null
        set(value) {
            if (field != value) {
                field = value
                text = "${reaction?.emoji?.emoji} ${reaction?.count}"
                if (reaction?.selected == true) {
                    rectPaint.color = viewSelectedBackgroundColor
                    textPaint.color = textSelectedColor
                }
                requestLayout()
            }
        }

    private var text: String = "${reaction?.emoji?.emoji} ${reaction?.count}"
        set(value) {
            if (field != value) {
                field = value
                requestLayout()
            }
        }

    private val rectPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint = Paint()

    private val viewRect = RectF()

    private val textPoint = PointF()

    private val textBounds = Rect()
    private val textBoundsNoEmoji = Rect()

    init {
        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.ReactionView).apply {
                viewBackgroundColor = getColor(R.styleable.ReactionView_rv_background_color, viewBackgroundColor)
                viewSelectedBackgroundColor = getColor(R.styleable.ReactionView_rv_selected_background_color, viewSelectedBackgroundColor)
                textColor = getColor(R.styleable.ReactionView_rv_text_color, textColor)
                textSelectedColor = getColor(R.styleable.ReactionView_rv_selected_text_color, textSelectedColor)
                cornerRadius = getDimension(R.styleable.ReactionView_rv_corner_radius, cornerRadius)
                paddingHorizontal = getDimension(R.styleable.ReactionView_rv_padding_horizontal, paddingHorizontal)
                paddingVertical = getDimension(R.styleable.ReactionView_rv_padding_vertical, paddingVertical)
                fontSize = getDimension(R.styleable.ReactionView_rv_font_size, fontSize)
                recycle()
            }
        }
        setupPaints()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (w == 0) return
        with(viewRect) {
            left = 0F
            top = 0F
            right = w.toFloat()
            bottom = h.toFloat()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val specWidth = MeasureSpec.getSize(widthMeasureSpec)
        val specHeight = MeasureSpec.getSize(heightMeasureSpec)

        textPaint.getTextBounds(text, 0, text.length, textBounds)
        textPaint.getTextBounds(reaction?.count.toString(), 0, reaction?.count.toString().length, textBoundsNoEmoji)

        val defaultWidth = (textBounds.width() + paddingHorizontal * 2).toInt()
        val defaultHeight = (textBoundsNoEmoji.height() + paddingVertical * 2).toInt()

        val viewWidth = when (MeasureSpec.getMode(widthMeasureSpec)) {
            MeasureSpec.EXACTLY -> specWidth
            MeasureSpec.AT_MOST -> minOf(specWidth, defaultWidth)
            MeasureSpec.UNSPECIFIED -> defaultWidth
            else -> defaultWidth
        }

        val viewHeight = when (MeasureSpec.getMode(heightMeasureSpec)) {
            MeasureSpec.EXACTLY -> specHeight
            MeasureSpec.AT_MOST -> minOf(specHeight, defaultHeight)
            MeasureSpec.UNSPECIFIED -> defaultHeight
            else -> defaultHeight
        }

        setMeasuredDimension(viewWidth, viewHeight)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        val emptySpaceVertical = height - textBoundsNoEmoji.height()
        textPoint.set(
            width / 2F,
            emptySpaceVertical / 2F + textBoundsNoEmoji.height()
        )
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawRoundRect(viewRect, cornerRadius, cornerRadius, rectPaint)
        canvas.drawText(text, textPoint.x, textPoint.y, textPaint)
    }

    private fun setupPaints() {
        with(rectPaint) {
            style = Paint.Style.FILL
            color = if (reaction?.selected == true) {
                viewSelectedBackgroundColor
            } else {
                viewBackgroundColor
            }
        }
        with(textPaint) {
            color = if (reaction?.selected == true) {
                textSelectedColor
            } else {
                textColor
            }
            textAlign = Paint.Align.CENTER
            textSize = fontSize
        }
    }

    companion object {
        private const val DEFAULT_BACKGROUND_COLOR = R.color.dark_gray
        private const val SELECTED_BACKGROUND_COLOR = R.color.dark_gray_selected
        private const val DEFAULT_TEXT_COLOR = R.color.light_gray
        private const val SELECTED_TEXT_COLOR = R.color.light_gray_selected
        private const val DEFAULT_CORNER_RADIUS = 10
        private const val DEFAULT_PADDING_HORIZONTAL = 9
        private const val DEFAULT_PADDING_VERTICAL = 9
        private const val DEFAULT_FONT_SIZE = 14
    }
}