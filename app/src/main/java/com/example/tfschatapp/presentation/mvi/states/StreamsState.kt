package com.example.tfschatapp.presentation.mvi.states

import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.ViewTyped

data class StreamsState(
    val isLoading: Boolean = false,
    val allStreams: List<StreamUi> = emptyList(),
    val filteredStreamsWithTopics: List<ViewTyped> = emptyList(),
    val subscribedStreams: List<StreamUi> = emptyList(),
    val subscribedFilteredStreamsWithTopics: List<ViewTyped> = emptyList(),
    val error: Throwable? = null,
    val searchQuery: String? = null
)