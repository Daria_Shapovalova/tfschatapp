package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.actions.MessagesCachingMode
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import io.reactivex.rxjava3.core.Observable

class ParseMessageFromEventMiddleware : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.ParseMessageFromEventAction::class.java)
            .withLatestFrom(state) { action, currentState -> action to currentState }
            .flatMap { (action, state) ->
                val processNewMessageActions = mutableListOf<MessagesAction>()
                val messagesCachingMode = if (state.topic != null) {
                    MessagesCachingMode.CACHE_TOPIC
                } else MessagesCachingMode.CACHE_STREAM
                processNewMessageActions.add(
                    MessagesAction.CacheMessagesAction(
                        messagesCachingMode,
                        listOf(action.event.message!!.toMessageWithReactions(action.streamName, action.event.message.topic))
                    )
                )
                processNewMessageActions.add(MessagesAction.ScrollToBottomAction)
                Observable.fromIterable(processNewMessageActions)
            }
    }
}