package com.example.tfschatapp.presentation.modelui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class UpdateMessageRequest(
    val messageUi: MessageUi,
    val updateMessageRequestType: UpdateMessageRequestType
) : Parcelable

enum class UpdateMessageRequestType {
    ADD_REACTION,
    COPY_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE
}