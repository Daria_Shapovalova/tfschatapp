package com.example.tfschatapp.presentation.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import coil.load
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.modelui.UserUi

@BindingAdapter("channel_open")
fun ImageView.setImageResource(stream: StreamUi?) {
    stream?.let {
        if (it.isOpened) {
            setImageResource(R.drawable.ic_arrow_up)
        } else {
            setImageResource(R.drawable.ic_arrow_down)
        }
    }
}

@BindingAdapter("avatar_url")
fun ImageView.setImageUrl(user: UserUi?) {
    user?.let {
        load(it.avatarUrl)
    }
}

@BindingAdapter("status_circle_visibility")
fun View.setVisibility(user: UserUi?) {
    user?.let {
        visibility = if (it.presence != "offline") {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}

@BindingAdapter("status_circle_color")
fun View.setBackground(user: UserUi?) {
    user?.let {
        if (it.presence == "active") {
            setBackgroundResource(R.drawable.circle_green)
        } else {
            setBackgroundResource(R.drawable.circle_orange_)
        }
    }
}

@BindingAdapter("status_color")
fun TextView.setStatusTextColor(user: UserUi?) {
    user?.let {
        when (it.presence) {
            "active" -> setTextColor(ResourcesCompat.getColor(resources, R.color.grass_green, null))
            "idle" -> setTextColor(ResourcesCompat.getColor(resources, R.color.status_orange, null))
            else -> setTextColor(ResourcesCompat.getColor(resources, R.color.status_red, null))
        }
    }
}

@BindingAdapter("profile_name")
fun TextView.setProfileName(user: UserUi?) {
    user?.let {
        text = it.name
    }
}

@BindingAdapter("unread_count")
fun TextView.setUnreadMessagesCount(topic: TopicUi?) {
    topic?.let {
        text = resources.getString(R.string.unread_count, it.messagesCount)
        visibility = if (it.messagesCount == 0) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
}