package com.example.tfschatapp.presentation.modelui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Parcelize
@Serializable
data class ReactionUi(
    val uid: String,
    val messageId: String,
    val emoji: EmojiUi,
    var count: Int = 0,
    var selected: Boolean = false
): Parcelable {
    fun update(operation: String, fromCurrentUser: Boolean) {
        when (operation) {
            EVENT_REACTION_ADD -> {
                count++
                if (fromCurrentUser) {
                    selected = true
                }
            }
            EVENT_REACTION_REMOVE -> {
                count--
                if (fromCurrentUser) {
                    selected = false
                }
            }
        }
    }

    companion object {
        private const val EVENT_REACTION_ADD = "add"
        private const val EVENT_REACTION_REMOVE = "remove"
    }
}