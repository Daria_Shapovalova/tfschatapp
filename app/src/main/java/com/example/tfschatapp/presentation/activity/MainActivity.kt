package com.example.tfschatapp.presentation.activity

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.actions.MainAction
import com.example.tfschatapp.presentation.mvi.states.MainState
import com.example.tfschatapp.presentation.viewmodels.MainViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jakewharton.rxrelay3.PublishRelay
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), ActivityWithBottomNavigation, MviView<MainAction, MainState> {
    override val actions: PublishRelay<MainAction> = PublishRelay.create()
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavigationView: BottomNavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        val appBarConfiguration = AppBarConfiguration(setOf(R.id.channelsFragment, R.id.contactsFragment, R.id.profileFragment))
        setupActionBarWithNavController(navController, appBarConfiguration)

        bottomNavigationView.setupWithNavController(navController)

        viewModel.bind(this)
        loadUsersData()
    }

    override fun hideBottomNavigation() {
        val bottomNavigation = findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavigation.visibility = View.GONE
    }

    override fun showBottomNavigation() {
        val bottomNavigation: BottomNavigationView? = findViewById(R.id.nav_view)
        bottomNavigation?.visibility = View.VISIBLE
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    fun loadUsersData() {
        actions.accept(MainAction.GetCurrentUserAction)
        actions.accept(MainAction.LoadUsersFromNetworkAction)
        actions.accept(MainAction.ReportUserPresenceAction)
    }

    override fun render(state: MainState) { }
}