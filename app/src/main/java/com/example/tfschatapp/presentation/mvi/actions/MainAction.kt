package com.example.tfschatapp.presentation.mvi.actions

import com.example.tfschatapp.database.modeldb.UserDb

sealed class MainAction {
    object LoadUsersFromNetworkAction : MainAction()
    data class LoadUsersFromNetworkFailureAction(val error: Throwable) : MainAction()

    data class CacheUsersAction(val users: List<UserDb>) : MainAction()
    object CacheUsersSuccessAction : MainAction()
    data class CacheUsersFailureAction(val error: Throwable) : MainAction()

    object GetCurrentUserAction : MainAction()
    object GetCurrentUserSuccessAction : MainAction()
    data class GetCurrentUserFailureAction(val error: Throwable) : MainAction()

    object ReportUserPresenceAction : MainAction()
    data class ReportUserPresenceFailureAction(val error: Throwable) : MainAction()

    data class CacheUserPresenceAction(val usersPresence: Map<String, String>) : MainAction()
    object CacheUserPresenceSuccessAction : MainAction()
    data class CacheUserPresenceFailureAction(val error: Throwable) : MainAction()
}