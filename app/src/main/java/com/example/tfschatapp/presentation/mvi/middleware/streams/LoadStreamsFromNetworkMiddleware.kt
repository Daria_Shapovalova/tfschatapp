package com.example.tfschatapp.presentation.mvi.middleware.streams

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.repository.StreamsRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class LoadStreamsFromNetworkMiddleware(private val streamsRepository: StreamsRepository) : Middleware<StreamsAction, StreamsState> {
    override fun bind(actions: Observable<StreamsAction>, state: Observable<StreamsState>): Observable<StreamsAction> {
        return actions.ofType(StreamsAction.LoadStreamsFromNetworkAction::class.java)
            .flatMap { action ->
                streamsRepository.loadAllStreamsFromNetwork()!!
                    .subscribeOn(Schedulers.io())
                    .map<StreamsAction> { result -> StreamsAction.LoadStreamsFromNetworkSuccessAction(result) }
                    .onErrorReturn { error -> StreamsAction.LoadStreamsFromNetworkFailureAction(error) }
                    .toObservable()
            }
    }
}