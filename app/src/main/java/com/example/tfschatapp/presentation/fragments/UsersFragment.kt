package com.example.tfschatapp.presentation.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.tfschatapp.R
import com.example.tfschatapp.databinding.FragmentContactsBinding
import com.example.tfschatapp.presentation.activity.ActivityWithBottomNavigation
import com.example.tfschatapp.presentation.activity.MainActivity
import com.example.tfschatapp.presentation.modelui.UserUi
import com.example.tfschatapp.presentation.modelui.ViewTyped
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.actions.UsersAction
import com.example.tfschatapp.presentation.mvi.states.UsersState
import com.example.tfschatapp.presentation.recyclerview.AsyncAdapter
import com.example.tfschatapp.presentation.recyclerview.ChatHolderFactory
import com.example.tfschatapp.presentation.recyclerview.holders.ContactClickListener
import com.example.tfschatapp.presentation.viewmodels.UsersViewModel
import com.jakewharton.rxrelay3.PublishRelay
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.ObservableOnSubscribe
import java.util.*
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class UsersFragment : Fragment(), MviView<UsersAction, UsersState> {
    override val actions: PublishRelay<UsersAction> = PublishRelay.create()
    private val viewModel: UsersViewModel by viewModels()

    private var _binding: FragmentContactsBinding? = null
    private val binding get() = _binding!!

    private lateinit var holderFactory: ChatHolderFactory
    private lateinit var adapter: AsyncAdapter<ViewTyped>

    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        _binding = FragmentContactsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showBottomNavigation()
        setUpRecyclerView()
        viewModel.bind(this)
        actions.accept(UsersAction.LoadUsersAction)
        setUpRetryButton()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        setUpSearchView(menu, inflater)
        setUpObservableForSearchView()
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun hideShimmerPlaceholder() {
        binding.shimmerPlaceholderLayout.stopShimmer()
        binding.shimmerPlaceholderLayout.visibility = View.GONE
    }

    private fun showBottomNavigation() {
        (activity as ActivityWithBottomNavigation).showBottomNavigation()
    }

    private fun setUpRecyclerView() {
        val contactClickListener = ContactClickListener { user ->
            navigateToUserProfile(user)
        }
        holderFactory = ChatHolderFactory(contactClick = contactClickListener)
        adapter = AsyncAdapter(holderFactory)
        binding.contactsRecyclerView.adapter = adapter
    }

    private fun setUpRetryButton() {
        binding.errorNetworkLayout.networkErrorRetryButton.setOnClickListener {
            (activity as MainActivity).loadUsersData()
        }
    }

    override fun render(state: UsersState) {
        if (!state.isLoading) {
            hideShimmerPlaceholder()
        }
        if (state.allUsers.isEmpty()) {
            binding.errorNetworkLayout.root.visibility = View.VISIBLE
        } else {
            binding.errorNetworkLayout.root.visibility = View.GONE
        }
        adapter.items = state.filteredUsers.sortedBy { user -> user.presence }
        scrollRecyclerViewToTop()
    }

    private fun scrollRecyclerViewToTop() {
        binding.contactsRecyclerView.scrollToPosition(0)
    }

    private fun navigateToUserProfile(user: UserUi?) {
        if (user != null) {
            this.findNavController().navigate(UsersFragmentDirections.actionShowContactDetails(user))
        }
    }

    private fun setUpSearchView(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search, menu)
        searchView = menu.findItem(R.id.search_field)?.actionView as SearchView
        searchView.queryHint = getString(R.string.users_search_query_hint)
    }

    private fun setUpObservableForSearchView() {
        Observable.create(ObservableOnSubscribe<String> { subscriber ->
            setOnQueryTextListener(subscriber)
        })
            .map { text -> text.toLowerCase(Locale.ROOT).trim() }
            .debounce(SEARCH_TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { text ->
                actions.accept(UsersAction.FilterUsersByQueryAction(text))
            }
    }

    private fun setOnQueryTextListener(subscriber: ObservableEmitter<String>) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                subscriber.onNext(newText!!)
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                subscriber.onNext(query!!)
                return false
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        viewModel.unbind()
    }

    companion object {
        private const val SEARCH_TIMEOUT_MILLISECONDS = 250L
    }
}