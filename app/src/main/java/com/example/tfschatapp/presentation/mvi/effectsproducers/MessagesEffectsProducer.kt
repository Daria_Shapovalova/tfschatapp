package com.example.tfschatapp.presentation.mvi.effectsproducers

import com.example.tfschatapp.presentation.mvi.EffectsProducer
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.uieffects.MessagesUiEffect

class MessagesEffectsProducer : EffectsProducer<MessagesAction, MessagesUiEffect> {
    override fun produce(action: MessagesAction): MessagesUiEffect {
        return when (action) {
            is MessagesAction.LoadNextMessagesPageFailureAction -> MessagesUiEffect.LoadNextPageError(action.error)
            is MessagesAction.SendMessageSuccessAction -> MessagesUiEffect.SendMessageSuccess
            is MessagesAction.SendMessageFailureAction -> MessagesUiEffect.SendMessageError(action.error)
            is MessagesAction.AddReactionFailureAction -> MessagesUiEffect.AddReactionError(action.error)
            is MessagesAction.RemoveReactionFailureAction -> MessagesUiEffect.RemoveReactionError(action.error)
            is MessagesAction.NavigateToUserProfile -> MessagesUiEffect.NavigateToUserProfile(action.user)
            is MessagesAction.ScrollToBottomAction -> MessagesUiEffect.ScrollToBottom
            is MessagesAction.SelectReactionAction -> MessagesUiEffect.OpenPickEmojiFragment(action.messageId)
            is MessagesAction.CopyToClipboardAction -> MessagesUiEffect.CopyMessageToClipboard(action.messageText)
            is MessagesAction.DeleteMessageFailureAction -> MessagesUiEffect.DeleteMessageError(action.error)
            is MessagesAction.OpenMessageEditorAction -> MessagesUiEffect.OpenMessageEditorAction(action.message)
            is MessagesAction.EditMessageSuccessAction -> MessagesUiEffect.EditMessageSuccess
            is MessagesAction.EditMessageFailureAction -> MessagesUiEffect.EditMessageError(action.error)
            else -> MessagesUiEffect.NoEffect
        }
    }
}