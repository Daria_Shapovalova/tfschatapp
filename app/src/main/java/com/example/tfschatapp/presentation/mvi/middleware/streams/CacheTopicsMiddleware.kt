package com.example.tfschatapp.presentation.mvi.middleware.streams

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.repository.StreamsRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class CacheTopicsMiddleware(private val streamsRepository: StreamsRepository) : Middleware<StreamsAction, StreamsState> {
    override fun bind(actions: Observable<StreamsAction>, state: Observable<StreamsState>): Observable<StreamsAction> {
        return actions.ofType(StreamsAction.CacheTopicsAction::class.java)
            .flatMap { action ->
                streamsRepository.saveTopicsToDatabase(action.topics)
                    .subscribeOn(Schedulers.io())
                    .toSingleDefault(true)
                    .map<StreamsAction> { result -> StreamsAction.LoadUnreadMessageCountsAction }
                    .onErrorReturn { error -> StreamsAction.CacheTopicsFailureAction(error) }
                    .toObservable()
            }
    }
}