package com.example.tfschatapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import com.example.tfschatapp.presentation.mvi.MviViewWithUiEffects
import com.example.tfschatapp.presentation.mvi.StoreWithUiEffects
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.effectsproducers.MessagesEffectsProducer
import com.example.tfschatapp.presentation.mvi.middleware.messages.*
import com.example.tfschatapp.presentation.mvi.reducers.MessagesReducer
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.presentation.mvi.uieffects.MessagesUiEffect
import com.example.tfschatapp.repository.MessagesRepository
import com.example.tfschatapp.repository.SharedPreferencesDao
import com.example.tfschatapp.repository.StreamsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class StreamDiscussionViewModel @Inject constructor(
    messagesRepository: MessagesRepository,
    streamsRepository: StreamsRepository,
    sharedPreferencesDao: SharedPreferencesDao
) :
    ViewModel() {
    val currentUserId: String? = sharedPreferencesDao.loadCurrentUserId()

    private val store = StoreWithUiEffects(
        MessagesReducer(), listOf(
            ShowCachedMessagesForStreamMiddleware(messagesRepository),
            LoadFirstMessagesPageMiddleware(messagesRepository),
            LoadNextMessagesPageMiddleware(messagesRepository),
            CacheFirstMessagesPageMiddleware(),
            CacheNextMessagesPageMiddleware(),
            SendMessageMiddleware(messagesRepository),
            RegisterEventsQueueMiddleware(messagesRepository),
            RegisterEventsQueueSuccessMiddleware(),
            GetEventsFromQueueMiddleware(messagesRepository),
            ParseEventsMiddleware(),
            ParseMessageFromEventMiddleware(),
            AddReactionMiddleware(messagesRepository),
            RemoveReactionMiddleware(messagesRepository),
            ParseReactionFromEventMiddleware(messagesRepository),
            MarkMessagesInStreamAsReadMiddleware(messagesRepository),
            MarkCachedMessagesInStreamAsReadMiddleware(streamsRepository),
            CacheMessagesMiddleware(messagesRepository),
            DeleteMessageMiddleware(messagesRepository),
            ParseDeletedMessageFromEventMiddleware(messagesRepository),
            EditMessageMiddleware(messagesRepository),
            ParseEditedMessageContentFromEventMiddleware(messagesRepository),
            ParseEditedMessageTopicFromEventMiddleware(messagesRepository)
        ), MessagesState(currentUserId = currentUserId),
        MessagesEffectsProducer()
    )

    private val wiring = store.wire()
    private var viewBinding: Disposable? = null

    fun bind(view: MviViewWithUiEffects<MessagesAction, MessagesState, MessagesUiEffect>) {
        viewBinding = store.bind(view)
    }

    fun unbind() {
        viewBinding?.dispose()
    }

    override fun onCleared() {
        wiring.dispose()
        super.onCleared()
    }
}