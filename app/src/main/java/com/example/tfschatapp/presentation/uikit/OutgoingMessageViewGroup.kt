package com.example.tfschatapp.presentation.uikit

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.core.view.*
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.extensions.layout
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.uikit.flexbox.FlexBoxAdapter
import com.example.tfschatapp.presentation.uikit.flexbox.FlexBoxLayout
import com.example.tfschatapp.presentation.uikit.flexbox.ReactionAdapter

class OutgoingMessageViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : ViewGroup(context, attrs, defStyleAttr, defStyleRes), ClickableMessage {
    private val messageBoxViewGroup: OutgoingMessageBoxViewGroup
    private val reactionsFlexBoxLayout: FlexBoxLayout

    private val messageBoxRect = Rect()
    private val reactionsFlexBoxRect = Rect()

    private val adapter = ReactionAdapter()

    override var message: MessageUi? = null
        set(value) {
            if (field != value) {
                field = value
                messageBoxViewGroup.messageText = message?.messageText
                reactionsFlexBoxLayout.visibility = if (message?.reactions!!.isEmpty()) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
                reactionsFlexBoxLayout.items = message?.reactions!!
                requestLayout()
            }
        }

    var messageLongClickListener: ((View) -> Boolean)? = null
        set(value) {
            if (field != value) {
                field = value
                messageBoxViewGroup.setOnLongClickListener(messageLongClickListener)
            }
        }

    var addReactionClickListener: ((View) -> Unit)? = null
        set(value) {
            if (field != value) {
                field = value
                reactionsFlexBoxLayout.addIconClickListener = value
            }
        }

    var reactionClickListener: ((View) -> Unit)? = null
        set(value) {
            if (field != value) {
                field = value
                reactionsFlexBoxLayout.itemClickListener = value
            }
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.outgoing_message_view_group, this, true)
        messageBoxViewGroup = findViewById(R.id.messageBox)
        reactionsFlexBoxLayout = findViewById(R.id.reactionsFlexBox)
        reactionsFlexBoxLayout.adapter = adapter as FlexBoxAdapter<Any>
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val messageBoxLayoutParams = messageBoxViewGroup.layoutParams as MarginLayoutParams
        val reactionsFlexBoxLayoutParams = reactionsFlexBoxLayout.layoutParams as MarginLayoutParams

        measureChildWithMargins(messageBoxViewGroup, widthMeasureSpec, 0, heightMeasureSpec, 0)
        val messageBoxHeight = messageBoxViewGroup.measuredHeight + messageBoxLayoutParams.topMargin + messageBoxLayoutParams.bottomMargin
        val messageBoxWidth = messageBoxViewGroup.measuredWidth + messageBoxLayoutParams.leftMargin + messageBoxLayoutParams.rightMargin

        if (message?.reactions?.isNotEmpty() == true) {
            val widthUsed = MeasureSpec.getSize(widthMeasureSpec) - messageBoxWidth

            measureChildWithMargins(reactionsFlexBoxLayout, widthMeasureSpec, widthUsed, heightMeasureSpec, messageBoxHeight)
            val reactionsFlexBoxHeight =
                reactionsFlexBoxLayout.measuredHeight + reactionsFlexBoxLayoutParams.topMargin + reactionsFlexBoxLayoutParams.bottomMargin
            val reactionsFlexBoxWidth =
                reactionsFlexBoxLayout.measuredWidth + reactionsFlexBoxLayoutParams.leftMargin + reactionsFlexBoxLayoutParams.rightMargin

            setMeasuredDimension(
                resolveSize(maxOf(messageBoxWidth, reactionsFlexBoxWidth), widthMeasureSpec),
                resolveSize(messageBoxHeight + reactionsFlexBoxHeight, heightMeasureSpec)
            )
        } else {
            setMeasuredDimension(
                resolveSize(messageBoxWidth, widthMeasureSpec),
                resolveSize(messageBoxHeight, heightMeasureSpec)
            )
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        messageBoxRect.right = measuredWidth - messageBoxViewGroup.marginEnd
        messageBoxRect.left = messageBoxRect.right - messageBoxViewGroup.measuredWidth
        messageBoxRect.top = messageBoxViewGroup.marginTop
        messageBoxRect.bottom = messageBoxViewGroup.top + messageBoxViewGroup.measuredHeight
        messageBoxViewGroup.layout(messageBoxRect)

        if (message?.reactions?.isNotEmpty() == true) {
            reactionsFlexBoxRect.left = messageBoxRect.left
            reactionsFlexBoxRect.right = measuredWidth - reactionsFlexBoxLayout.marginEnd
            reactionsFlexBoxRect.top = messageBoxRect.bottom + messageBoxViewGroup.marginBottom + reactionsFlexBoxLayout.marginTop
            reactionsFlexBoxRect.bottom = reactionsFlexBoxRect.top + reactionsFlexBoxLayout.measuredHeight
            reactionsFlexBoxLayout.layout(reactionsFlexBoxRect)
        }
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(MATCH_PARENT, WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?) = MarginLayoutParams(context, attrs)

    override fun generateLayoutParams(p: LayoutParams?): LayoutParams = MarginLayoutParams(p)
}