package com.example.tfschatapp.presentation.recyclerview.holders

import android.view.View
import com.example.tfschatapp.databinding.ItemContactBinding
import com.example.tfschatapp.presentation.modelui.UserUi
import com.example.tfschatapp.presentation.recyclerview.BaseViewHolder

class ContactViewHolder(view: View, private val contactClick: ContactClickListener? = null) : BaseViewHolder<UserUi>(view) {
    private var contactViewBinding: ItemContactBinding = ItemContactBinding.bind(view)

    override fun bind(item: UserUi) {
        contactViewBinding.contact = item
        contactViewBinding.clickListener = contactClick
    }
}

class ContactClickListener(val clickListener: (user: UserUi) -> Unit) {
    fun onClick(user: UserUi) = clickListener(user)
}