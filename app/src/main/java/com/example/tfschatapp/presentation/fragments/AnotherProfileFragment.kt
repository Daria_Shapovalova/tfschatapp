package com.example.tfschatapp.presentation.fragments

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.tfschatapp.R
import com.example.tfschatapp.databinding.FragmentAnotherProfileBinding
import com.example.tfschatapp.presentation.activity.ActivityWithBottomNavigation
import com.example.tfschatapp.presentation.mvi.MviView
import com.example.tfschatapp.presentation.mvi.actions.ProfileAction
import com.example.tfschatapp.presentation.mvi.states.ProfileState
import com.example.tfschatapp.presentation.viewmodels.AnotherUserProfileViewModel
import com.jakewharton.rxrelay3.PublishRelay
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AnotherProfileFragment : Fragment(), MviView<ProfileAction, ProfileState> {
    override val actions: PublishRelay<ProfileAction> = PublishRelay.create()
    private val viewModel: AnotherUserProfileViewModel by viewModels()

    private var _binding: FragmentAnotherProfileBinding? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_another_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpStatusBar()
        setUpActionBar()
        hideBottomNavigation()

        val user = AnotherProfileFragmentArgs.fromBundle(requireArguments()).selectedUser

        viewModel.bind(this)
        actions.accept(ProfileAction.LoadProfileAction(user.uid))
    }

    private fun setUpStatusBar() {
        activity?.window?.statusBarColor = ContextCompat.getColor(requireContext(), R.color.dark_gray_navigation)
    }

    private fun setUpActionBar() {
        (activity as? AppCompatActivity)?.supportActionBar?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.dark_gray_navigation
                )
            )
        )
    }

    private fun hideBottomNavigation() {
        (activity as ActivityWithBottomNavigation).hideBottomNavigation()
    }

    override fun render(state: ProfileState) {
        state.userProfile. let {
            binding.contact = it
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        viewModel.unbind()
    }
}