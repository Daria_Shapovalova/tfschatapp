package com.example.tfschatapp.presentation.uikit

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.core.view.*
import coil.load
import com.example.tfschatapp.R
import com.google.android.material.imageview.ShapeableImageView
import com.example.tfschatapp.presentation.extensions.layout
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.recyclerview.holders.ContactClickListener
import com.example.tfschatapp.presentation.uikit.flexbox.FlexBoxAdapter
import com.example.tfschatapp.presentation.uikit.flexbox.FlexBoxLayout
import com.example.tfschatapp.presentation.uikit.flexbox.ReactionAdapter

class IncomingMessageViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : ViewGroup(context, attrs, defStyleAttr, defStyleRes), ClickableMessage {
    private val circleImageView: ShapeableImageView
    private val messageBoxViewGroup: IncomingMessageBoxViewGroup
    private val reactionsFlexBoxLayout: FlexBoxLayout

    private val avatarRect = Rect()
    private val messageBoxRect = Rect()
    private val reactionsFlexBoxRect = Rect()

    private val adapter = ReactionAdapter()

    override var message: MessageUi? = null
        set(value) {
            if (field != value) {
                field = value
                circleImageView.load(message?.author?.avatarUrl)
                messageBoxViewGroup.name = message?.author?.name
                messageBoxViewGroup.messageText = message?.messageText
                reactionsFlexBoxLayout.visibility = if (message?.reactions!!.isEmpty()) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
                reactionsFlexBoxLayout.items = message?.reactions!!
                requestLayout()
            }
        }

    var messageLongClickListener: ((View) -> Boolean)? = null
        set(value) {
            if (field != value) {
                field = value
                messageBoxViewGroup.setOnLongClickListener(messageLongClickListener)
            }
        }

    var addReactionClickListener: ((View) -> Unit)? = null
        set(value) {
            if (field != value) {
                field = value
                reactionsFlexBoxLayout.addIconClickListener = value
            }
        }

    var reactionClickListener: ((View) -> Unit)? = null
        set(value) {
            if (field != value) {
                field = value
                reactionsFlexBoxLayout.itemClickListener = value
            }
        }

    var contactClickListener: ContactClickListener? = null
        set(value) {
            if (field != value) {
                field = value
                circleImageView.setOnClickListener { value?.onClick(message!!.author) }
            }
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.incoming_message_view_group, this, true)
        circleImageView = findViewById(R.id.avatarView)
        messageBoxViewGroup = findViewById(R.id.messageBox)
        reactionsFlexBoxLayout = findViewById(R.id.reactionsFlexBox)
        reactionsFlexBoxLayout.adapter = adapter as FlexBoxAdapter<Any>
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val circleImageViewLayoutParams = circleImageView.layoutParams as MarginLayoutParams
        val messageBoxLayoutParams = messageBoxViewGroup.layoutParams as MarginLayoutParams
        val reactionsFlexBoxLayoutParams = reactionsFlexBoxLayout.layoutParams as MarginLayoutParams

        measureChildWithMargins(circleImageView, widthMeasureSpec, 0, heightMeasureSpec, 0)
        val avatarHeight = circleImageView.measuredHeight + circleImageViewLayoutParams.topMargin + circleImageViewLayoutParams.bottomMargin
        val avatarWidth = circleImageView.measuredWidth + circleImageViewLayoutParams.leftMargin + circleImageViewLayoutParams.rightMargin

        measureChildWithMargins(messageBoxViewGroup, widthMeasureSpec, avatarWidth, heightMeasureSpec, 0)
        val messageBoxHeight = messageBoxViewGroup.measuredHeight + messageBoxLayoutParams.topMargin + messageBoxLayoutParams.bottomMargin
        val messageBoxWidth = messageBoxViewGroup.measuredWidth + messageBoxLayoutParams.leftMargin + messageBoxLayoutParams.rightMargin

        if (message?.reactions?.isNotEmpty() == true) {
            measureChildWithMargins(reactionsFlexBoxLayout, widthMeasureSpec, avatarWidth, heightMeasureSpec, messageBoxHeight)
            val reactionsFlexBoxHeight =
                reactionsFlexBoxLayout.measuredHeight + reactionsFlexBoxLayoutParams.topMargin + reactionsFlexBoxLayoutParams.bottomMargin
            val reactionsFlexBoxWidth =
                reactionsFlexBoxLayout.measuredWidth + reactionsFlexBoxLayoutParams.leftMargin + reactionsFlexBoxLayoutParams.rightMargin

            setMeasuredDimension(
                resolveSize(avatarWidth + maxOf(messageBoxWidth, reactionsFlexBoxWidth), widthMeasureSpec),
                resolveSize(maxOf(avatarHeight, messageBoxHeight + reactionsFlexBoxHeight), heightMeasureSpec)
            )

        } else
            setMeasuredDimension(
                resolveSize(avatarWidth + messageBoxWidth, widthMeasureSpec),
                resolveSize(maxOf(avatarHeight, messageBoxHeight), heightMeasureSpec)
            )
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        avatarRect.left = circleImageView.marginStart
        avatarRect.top = circleImageView.marginTop
        avatarRect.right = avatarRect.left + circleImageView.measuredWidth
        avatarRect.bottom = avatarRect.top + circleImageView.measuredHeight
        circleImageView.layout(avatarRect)

        messageBoxRect.left = avatarRect.right + circleImageView.marginEnd + messageBoxViewGroup.marginStart
        messageBoxRect.top = messageBoxViewGroup.marginTop
        messageBoxRect.right = messageBoxRect.left + messageBoxViewGroup.measuredWidth
        messageBoxRect.bottom = messageBoxViewGroup.top + messageBoxViewGroup.measuredHeight
        messageBoxViewGroup.layout(messageBoxRect)

        if (message?.reactions?.isNotEmpty() == true) {
            reactionsFlexBoxRect.left = avatarRect.right + circleImageView.marginEnd + reactionsFlexBoxLayout.marginStart
            reactionsFlexBoxRect.top = messageBoxRect.bottom + messageBoxViewGroup.marginBottom + reactionsFlexBoxLayout.marginTop
            reactionsFlexBoxRect.right = reactionsFlexBoxRect.left + reactionsFlexBoxLayout.measuredWidth
            reactionsFlexBoxRect.bottom = reactionsFlexBoxRect.top + reactionsFlexBoxLayout.measuredHeight
            reactionsFlexBoxLayout.layout(reactionsFlexBoxRect)
        }
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(MATCH_PARENT, WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?) = MarginLayoutParams(context, attrs)

    override fun generateLayoutParams(p: LayoutParams?): LayoutParams = MarginLayoutParams(p)
}