package com.example.tfschatapp.presentation.uikit

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import androidx.core.view.*
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.extensions.layout

class OutgoingMessageBoxViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : ViewGroup(context, attrs, defStyleAttr, defStyleRes) {
    var messageText: String? = null
        set(value) {
            if (field != value) {
                field = value
                messageTextView.text = messageText
                requestLayout()
            }
        }

    private val messageTextView: TextView
    private val messageTextRect = Rect()

    init {
        LayoutInflater.from(context).inflate(R.layout.outgoing_message_box_view_group, this, true)
        messageTextView = findViewById(R.id.text)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val textLayoutParams = messageTextView.layoutParams as MarginLayoutParams

        measureChildWithMargins(messageTextView, widthMeasureSpec, 0, heightMeasureSpec, 0)
        val messageTextHeight = messageTextView.measuredHeight + textLayoutParams.topMargin + textLayoutParams.bottomMargin
        val messageTextWidth = messageTextView.measuredWidth + textLayoutParams.leftMargin + textLayoutParams.rightMargin

        setMeasuredDimension(
            resolveSize(messageTextWidth, widthMeasureSpec),
            resolveSize(messageTextHeight, heightMeasureSpec)
        )
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        messageTextRect.left = messageTextView.marginStart
        messageTextRect.top = messageTextView.marginTop
        messageTextRect.right = messageTextRect.left + messageTextView.measuredWidth
        messageTextRect.bottom = messageTextRect.top + messageTextView.measuredHeight
        messageTextView.layout(messageTextRect)
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(MATCH_PARENT, WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?) = MarginLayoutParams(context, attrs)

    override fun generateLayoutParams(p: LayoutParams?): LayoutParams = MarginLayoutParams(p)
}