package com.example.tfschatapp.presentation.mvi.middleware.messages

import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.MessagesAction
import com.example.tfschatapp.presentation.mvi.states.MessagesState
import com.example.tfschatapp.repository.MessagesRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class RegisterEventsQueueMiddleware(private val messagesRepository: MessagesRepository) : Middleware<MessagesAction, MessagesState> {
    override fun bind(actions: Observable<MessagesAction>, state: Observable<MessagesState>): Observable<MessagesAction> {
        return actions.ofType(MessagesAction.RegisterEventQueueAction::class.java)
            .withLatestFrom(state) { action, currentState -> action to currentState }
            .flatMap { (action, state) ->
                if (state.topic != null) {
                    registerEventQueueForTopic(state.topic)
                } else {
                    registerEventQueueForStream(state.stream!!)
                }
            }
            .onErrorReturn { error -> MessagesAction.RegisterEventQueueFailureAction(error) }
    }

    private fun registerEventQueueForTopic(topic: TopicUi): Observable<MessagesAction> {
        return messagesRepository.registerEventQueueForTopic(topic.streamName, topic.name)
            .subscribeOn(Schedulers.io())
            .map<MessagesAction> { result -> MessagesAction.RegisterEventQueueSuccessAction(topic.streamName, topic.name, result) }
            .onErrorReturn { error -> MessagesAction.RegisterEventQueueFailureAction(error) }
            .toObservable()
    }

    private fun registerEventQueueForStream(stream: StreamUi): Observable<MessagesAction> {
        return messagesRepository.registerEventQueueForStream(stream.name)
            .subscribeOn(Schedulers.io())
            .map<MessagesAction> { result -> MessagesAction.RegisterEventQueueSuccessAction(stream.name, topicName = null, result) }
            .onErrorReturn { error -> MessagesAction.RegisterEventQueueFailureAction(error) }
            .toObservable()
    }
}