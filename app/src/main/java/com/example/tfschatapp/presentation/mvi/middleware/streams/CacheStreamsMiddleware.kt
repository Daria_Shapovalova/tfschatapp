package com.example.tfschatapp.presentation.mvi.middleware.streams

import com.example.tfschatapp.presentation.mvi.Middleware
import com.example.tfschatapp.presentation.mvi.actions.StreamsAction
import com.example.tfschatapp.presentation.mvi.states.StreamsState
import com.example.tfschatapp.repository.StreamsRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

class CacheStreamsMiddleware(private val streamsRepository: StreamsRepository) : Middleware<StreamsAction, StreamsState> {
    override fun bind(actions: Observable<StreamsAction>, state: Observable<StreamsState>): Observable<StreamsAction> {
        return actions.ofType(StreamsAction.CacheStreamsAction::class.java)
            .flatMap { action ->
                streamsRepository.saveStreamsToDatabase(action.streams)
                    .subscribeOn(Schedulers.io())
                    .toSingleDefault(true)
                    .map<StreamsAction> { result -> StreamsAction.LoadSubscribedStreamsIdsAction }
                    .onErrorReturn { error -> StreamsAction.CacheStreamsFailureAction(error) }
                    .toObservable()
            }
    }
}