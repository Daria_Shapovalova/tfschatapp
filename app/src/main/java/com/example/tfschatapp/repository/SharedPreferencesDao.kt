package com.example.tfschatapp.repository

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesDao(application: Context) {
    private val sharedPreferences: SharedPreferences = application.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = sharedPreferences.edit()

    fun saveCurrentUserId(id: String) {
        editor.apply {
            putString(CURRENT_USER_ID_KEY, id)
        }.apply()
    }

    fun loadCurrentUserId(): String? {
        return sharedPreferences.getString(CURRENT_USER_ID_KEY, null)
    }

    companion object {
        private const val SHARED_PREFERENCES_NAME = "shared_preferences"
        private const val CURRENT_USER_ID_KEY = "current_user_id"
    }
}