package com.example.tfschatapp.repository

import com.example.tfschatapp.database.dao.ContactsDao
import com.example.tfschatapp.database.modeldb.UserDb
import com.example.tfschatapp.network.ZulipApi
import com.example.tfschatapp.presentation.modelui.UserUi
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

class UsersRepository(private val usersDao: ContactsDao, private val zulipApi: ZulipApi, sharedPreferencesDao: SharedPreferencesDao) {

    val currentUserId: String? = sharedPreferencesDao.loadCurrentUserId()

    fun getUsersObservableFromDatabase(): Observable<List<UserUi>> {
        return usersDao.getAllUsers().map { users ->
            users
                .filter { it.uid != currentUserId }
                .map { userDb -> userDb.toUserUi() }
        }
    }

    fun saveUsersToDatabase(users: List<UserDb>): Completable {
        return Completable.fromAction {
            usersDao.insertUsers(users)
        }
    }

    fun getUsersFromNetwork(): Single<List<UserDb>>? {
        return zulipApi.retrofitService.getAllUsers().map { getUsersResponse ->
            getUsersResponse.members.map { user -> user.toUserDb() }
        }
    }

    fun getOwnUserIdFromNetwork(): Single<String>? {
        return zulipApi.retrofitService.getOwnUser().map { getOwnUserResponse ->
            getOwnUserResponse.userId.toString()
        }
    }

    fun reportUserPresence(): Single<Map<String, String>> {
        val currentTimestamp = System.currentTimeMillis() / 1000
        return zulipApi.retrofitService.reportUserPresence().map { reportUserPresenceResponse ->
            reportUserPresenceResponse.presences.mapValues {
                val timeStampDiff = currentTimestamp - it.value.aggregated.timestamp
                if (timeStampDiff < 300) it.value.aggregated.status
                else "offline"
            }
        }
    }

    fun saveUsersPresenceToDatabase(usersPresence: Map<String, String>): Completable {
        return Completable.fromAction {
            usersPresence.forEach { (userEmail, userPresence) ->
                usersDao.setUsersPresence(userEmail, userPresence)
            }
        }
    }

    fun getContactById(userId: String?): Observable<UserUi>? {
        userId?.let {
            return usersDao.getContactById(userId).switchMap { contact ->
                Observable.just(contact.toUserUi())
            }
        }
        return null
    }
}