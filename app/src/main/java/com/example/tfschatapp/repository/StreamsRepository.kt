package com.example.tfschatapp.repository

import com.example.tfschatapp.database.dao.ChannelDao
import com.example.tfschatapp.database.dao.ChannelWithTopicsDao
import com.example.tfschatapp.database.dao.TopicDao
import com.example.tfschatapp.database.modeldb.StreamDb
import com.example.tfschatapp.database.modeldb.TopicDb
import com.example.tfschatapp.network.ZulipApi
import com.example.tfschatapp.network.modelnetwork.CreateStreamRequest
import com.example.tfschatapp.network.modelnetwork.EditMessageRequest
import com.example.tfschatapp.network.modelnetwork.NarrowOperator
import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class StreamsRepository(
    private val streamsDao: ChannelDao, private val topicsDao: TopicDao, private val streamsWithTopicsDao: ChannelWithTopicsDao,
    private val zulipApi: ZulipApi
) {
    fun getStreamsFromDatabase(): Observable<List<StreamUi>> {
        return streamsWithTopicsDao.getChannelsWithTopics().map { channels ->
            channels
                .map { stream ->
                    val streamUi = stream.stream.toStreamUi()
                    stream.topics.sortedByDescending { it.messagesCount }.forEach { topic ->
                        val topicUi =
                            TopicUi(topic.uid, topic.name, topic.messagesCount, topic.color, stream.stream.uid, stream.stream.name, stream.stream.subscribed)
                        streamUi.topics.add(topicUi)
                    }
                    streamUi
                }
        }
    }

    fun loadAllStreamsFromNetwork(): Single<List<StreamDb>>? {
        return zulipApi.retrofitService.getAllStreams().map { getStreamsResponse ->
            getStreamsResponse.streams.map { stream -> stream.toStreamDatabase() }
        }
    }

    fun saveStreamsToDatabase(streams: List<StreamDb>): Completable {
        return Completable.fromAction {
            streamsDao.insertChannels(streams)
        }
    }

    fun loadSubscribedStreamIdsFromNetwork(): Single<List<String>>? {
        return zulipApi.retrofitService.getSubscribedStreams().map { getSubscriptionsResponse ->
            getSubscriptionsResponse.subscriptions.map { stream -> stream.uid.toString() }
        }
    }

    fun markStreamsAsSubscribed(streamIds: List<String>): Completable {
        return Completable.fromAction {
            streamIds.forEach { streamId ->
                streamsDao.markChannelAsSubscribed(streamId)
            }
        }
    }

    fun getTopicsInStreamFromNetwork(streamId: Int): Observable<List<TopicDb>>? {
        return zulipApi.retrofitService.getTopicsInStream(streamId).map { getTopicsResponse ->
            getTopicsResponse.topics.map { topic -> topic.toTopicDatabase(streamId) }
        }
    }

    fun saveTopicsToDatabase(topics: List<TopicDb>): Completable {
        return Completable.fromAction {
            topicsDao.insertTopics(topics)
        }
    }

    fun loadUnreadMessageCountsFromNetwork(): Single<Map<String, Int>> {
        val eventTypes = Json.encodeToString(arrayOf(EVENT_MESSAGE, EVENT_UPDATE_MESSAGE_FLAGS))
        return zulipApi.retrofitService.registerEventQueue(eventTypes = eventTypes).map { registerEventQueueResponse ->
            registerEventQueueResponse.unreadMessages!!.streams!!.map { topicWithUnreadCount ->
                "${topicWithUnreadCount.streamId} ${topicWithUnreadCount.topicName}" to topicWithUnreadCount.unreadMessageIds.count()
            }.toMap()
        }
    }

    fun saveUnreadMessageCountsToDatabase(unreadMessageCounts: Map<String, Int>): Completable {
        return Completable.fromAction {
            unreadMessageCounts.forEach { (topicUid, unreadCount) ->
                topicsDao.setUnreadMessageCount(topicUid, unreadCount)
            }
        }
    }

    fun updateStream(stream: StreamDb): Completable {
        return Completable.fromAction {
            streamsDao.updateChannel(stream)
        }
    }

    fun createStream(newStreamName: String): Completable {
        val createStreamRequest = Json.encodeToString(listOf(CreateStreamRequest(newStreamName)))
        return zulipApi.retrofitService.createStream(createStreamRequest)
    }

    fun getStreamId(streamName: String): Single<String> {
        return zulipApi.retrofitService.getStreamId(streamName).map { getStreamIdResponse ->
            getStreamIdResponse.streamId.toString()
        }
    }

    companion object {
        private const val EVENT_MESSAGE = "message"
        private const val EVENT_UPDATE_MESSAGE_FLAGS = "update_message_flags"
    }
}