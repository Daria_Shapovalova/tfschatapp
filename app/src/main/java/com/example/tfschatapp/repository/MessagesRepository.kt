package com.example.tfschatapp.repository

import com.example.tfschatapp.database.dao.MessageDao
import com.example.tfschatapp.database.dao.MessageWithReactionsDao
import com.example.tfschatapp.database.dao.ReactionDao
import com.example.tfschatapp.database.modeldb.MessageDb
import com.example.tfschatapp.database.modeldb.MessageWithReactions
import com.example.tfschatapp.database.modeldb.ReactionDb
import com.example.tfschatapp.network.ZulipApi
import com.example.tfschatapp.network.modelnetwork.*
import com.example.tfschatapp.presentation.modelui.MessageUi
import com.example.tfschatapp.presentation.modelui.StreamUi
import com.example.tfschatapp.presentation.modelui.TopicUi
import com.example.tfschatapp.presentation.mvi.actions.MessagesCachingMode
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class MessagesRepository(
    sharedPreferencesDao: SharedPreferencesDao,
    private val zulipApi: ZulipApi,
    private val messageDao: MessageDao,
    private val reactionDao: ReactionDao,
    private val messageWithReactionsDao: MessageWithReactionsDao
) {
    val currentUserId: String? = sharedPreferencesDao.loadCurrentUserId()

    fun loadMessagesInTopicFromNetwork(topic: TopicUi, anchor: Long): Single<Pair<Boolean, List<MessageWithReactions>>> {
        val requestNarrow = Json.encodeToString(
            listOf(
                NarrowOperator(REQUEST_NARROW_STREAM, topic.streamName),
                NarrowOperator(REQUEST_NARROW_TOPIC, topic.name)
            )
        )
        return zulipApi.retrofitService.getMessages(anchor = anchor, narrow = requestNarrow).map { getMessagesResponse ->
            Pair(getMessagesResponse.foundOldest, getMessagesResponse.messages.map { message ->
                message.toMessageWithReactions(topic.streamName, topic.name)
            })
        }
    }

    fun loadMessagesInStreamFromNetwork(stream: StreamUi, anchor: Long): Single<Pair<Boolean, List<MessageWithReactions>>> {
        val requestNarrow = Json.encodeToString(listOf(NarrowOperator(REQUEST_NARROW_STREAM, stream.name)))
        return zulipApi.retrofitService.getMessages(anchor = anchor, narrow = requestNarrow).map { getMessagesResponse ->
            Pair(getMessagesResponse.foundOldest, getMessagesResponse.messages.map { message ->
                message.toMessageWithReactions(stream.name, message.topic)
            })
        }
    }

    fun saveNewDataToDatabase(messagesCachingMode: MessagesCachingMode, messages: List<MessageWithReactions>) {
        saveMessagesToDatabase(messagesCachingMode, messages.map { it.message })
        saveReactionsToDatabase(messages.flatMap { it.reactions })
    }

    private fun saveMessagesToDatabase(messagesCachingMode: MessagesCachingMode, messages: List<MessageDb>) {
        Completable.fromAction { messageDao.insertMessages(messages) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (messagesCachingMode) {
                    MessagesCachingMode.CACHE_TOPIC -> clearCacheForTopic(messages[0].topicId)
                    MessagesCachingMode.CACHE_STREAM -> clearCacheForStream(messages[0].streamName)
                }
            }
    }

    private fun saveReactionsToDatabase(reactions: List<ReactionDb>) {
        Completable.fromAction { reactionDao.insertReactions(reactions) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun clearCacheForTopic(topicId: String) {
        messageDao.getIdsForMessagesInTopicToBeRemoved(topicId, MESSAGES_CACHE_LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { messageIds ->
                removeMessagesFromDatabase(messageIds)
                removeReactionsForMessagesFromDatabase(messageIds)
            }
    }

    private fun clearCacheForStream(streamName: String) {
        messageDao.getIdsForMessagesInStreamToBeRemoved(streamName, MESSAGES_CACHE_LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { messageIds ->
                removeMessagesFromDatabase(messageIds)
                removeReactionsForMessagesFromDatabase(messageIds)
            }
    }

    private fun removeMessagesFromDatabase(messageIds: List<String>) {
        messageIds.forEach { messageId ->
            Completable.fromAction { messageDao.removeMessage(messageId) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    private fun removeReactionsForMessagesFromDatabase(messageIds: List<String>) {
        messageIds.forEach { messageId ->
            Completable.fromAction { reactionDao.deleteReactionsForMessage(messageId) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    fun getAllMessagesInTopicFromDatabase(topicId: String): Single<List<MessageUi>> {
        return messageWithReactionsDao.getMessagesWithReactionsInTopic(topicId).map { messagesWithReactions ->
            messagesWithReactions.map { messageWithReactions -> messageWithReactions.toMessageUi(currentUserId.toString()) }
        }
    }

    fun getAllMessagesInStreamFromDatabase(streamName: String): Single<List<MessageUi>> {
        return messageWithReactionsDao.getMessagesWithReactionsInStream(streamName).map { messagesWithReactions ->
            messagesWithReactions.map { messageWithReactions -> messageWithReactions.toMessageUi(currentUserId.toString()) }
        }
    }

    fun markMessagesAsReadInTopic(topic: TopicUi): Completable {
        return zulipApi.retrofitService.markMessagesAsReadInTopic(topic.streamId.toInt(), topic.name)
    }

    fun markMessagesAsReadInStream(stream: StreamUi): Completable {
        return zulipApi.retrofitService.markMessagesAsReadInStream(stream.uid.toInt())
    }

    fun registerEventQueueForTopic(streamName: String, topicName: String): Single<RegisterEventQueueResponse> {
        val eventTypes = Json.encodeToString(arrayOf(EVENT_MESSAGE, EVENT_REACTION, EVENT_DELETE_MESSAGE, EVENT_UPDATE_MESSAGE))
        val requestNarrow = Json.encodeToString(arrayOf(arrayOf(REQUEST_NARROW_STREAM, streamName), arrayOf(REQUEST_NARROW_TOPIC, topicName)))
        return zulipApi.retrofitService.registerEventQueue(true, eventTypes, requestNarrow)
    }

    fun registerEventQueueForStream(streamName: String): Single<RegisterEventQueueResponse> {
        val eventTypes = Json.encodeToString(arrayOf(EVENT_MESSAGE, EVENT_REACTION, EVENT_DELETE_MESSAGE, EVENT_UPDATE_MESSAGE))
        val requestNarrow = Json.encodeToString(arrayOf(arrayOf(REQUEST_NARROW_STREAM, streamName)))
        return zulipApi.retrofitService.registerEventQueue(true, eventTypes, requestNarrow)
    }

    fun getEventsFromQueue(queueId: String, lastEventId: Int): Single<GetEventsFromQueueResponse> {
        return zulipApi.retrofitService.getEventsFromQueue(queueId, lastEventId)
    }

    fun sendMessage(message: SendMessageRequest): Single<SendMessageResponse> {
        return zulipApi.retrofitService.sendMessage(streamName = message.streamName, topicName = message.topicName, content = message.messageContent)
    }

    fun addReaction(reaction: AddOrRemoveReactionRequest): Completable {
        return zulipApi.retrofitService.addReaction(reaction.messageId, reaction.emojiName)
    }

    fun removeReaction(reaction: AddOrRemoveReactionRequest): Completable {
        return zulipApi.retrofitService.removeReaction(reaction.messageId, reaction.emojiName)
    }

    fun addReactionToDatabase(reaction: ReactionDb): Completable {
        return Completable.fromAction { reactionDao.insertReaction(reaction) }
    }

    fun removeReactionFromDatabase(reaction: ReactionDb): Completable {
        return Completable.fromAction { reactionDao.deleteReaction(reaction.uid) }
    }

    fun deleteMessage(messageId: String): Completable {
        return zulipApi.retrofitService.deleteMessage(messageId.toInt())
    }

    fun removeMessageFromDatabaseById(messageId: String): Completable {
        return Completable.fromAction { messageDao.removeMessage(messageId) }
    }

    fun editMessage(request: EditMessageRequest): Completable {
        return zulipApi.retrofitService.editMessage(messageId = request.messageId.toInt(), topicName = request.topicName, content = request.messageContent)
    }

    fun updateMessageContentInDatabase(messageId: String, newMessageContent: String): Completable {
        return Completable.fromAction { messageDao.updateMessageContent(messageId, newMessageContent) }
    }

    fun updateMessageTopicInDatabase(messageId: String, topicName: String, messageStreamName: String): Completable {
        val topicId = "$messageStreamName $topicName"
        return Completable.fromAction { messageDao.updateMessageTopic(messageId, topicId, topicName) }
    }

    companion object {
        private const val EVENT_MESSAGE = "message"
        private const val EVENT_REACTION = "reaction"
        private const val EVENT_DELETE_MESSAGE = "delete_message"
        private const val EVENT_UPDATE_MESSAGE = "update_message"
        private const val REQUEST_NARROW_STREAM = "stream"
        private const val REQUEST_NARROW_TOPIC = "topic"
        private const val MESSAGES_CACHE_LIMIT = 50
    }
}