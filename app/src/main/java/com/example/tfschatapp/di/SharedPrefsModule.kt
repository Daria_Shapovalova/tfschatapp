package com.example.tfschatapp.di

import android.content.Context
import com.example.tfschatapp.repository.SharedPreferencesDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SharedPrefsModule {
    @Provides
    @Singleton
    fun provideSharedPreferencesDao(@ApplicationContext context: Context): SharedPreferencesDao {
        return SharedPreferencesDao(context)
    }
}