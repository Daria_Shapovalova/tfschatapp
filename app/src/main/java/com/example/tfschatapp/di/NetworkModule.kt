package com.example.tfschatapp.di

import com.example.tfschatapp.network.ZulipApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    @Singleton
    fun provideZulipApi(): ZulipApi {
        return ZulipApi()
    }
}