package com.example.tfschatapp.di

import android.content.Context
import androidx.room.Room
import com.example.tfschatapp.database.ChatDatabase
import com.example.tfschatapp.database.DATABASE_NAME
import com.example.tfschatapp.database.dao.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): ChatDatabase {
        return Room.databaseBuilder(
            context,
            ChatDatabase::class.java,
            DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    fun provideMessageDao(chatDatabase: ChatDatabase): MessageDao {
        return chatDatabase.messageDao
    }

    @Provides
    @Singleton
    fun provideReactionDao(chatDatabase: ChatDatabase): ReactionDao {
        return chatDatabase.reactionDao
    }

    @Provides
    @Singleton
    fun provideMessageWithReactionsDao(chatDatabase: ChatDatabase): MessageWithReactionsDao {
        return chatDatabase.messageWithReactionsDao
    }

    @Provides
    @Singleton
    fun provideUserDao(chatDatabase: ChatDatabase): ContactsDao {
        return chatDatabase.userDao
    }

    @Provides
    @Singleton
    fun provideStreamDao(chatDatabase: ChatDatabase): ChannelDao {
        return chatDatabase.channelDao
    }

    @Provides
    @Singleton
    fun provideTopicDao(chatDatabase: ChatDatabase): TopicDao {
        return chatDatabase.topicDao
    }

    @Provides
    @Singleton
    fun provideChannelWithTopicsDao(chatDatabase: ChatDatabase): ChannelWithTopicsDao {
        return chatDatabase.channelWithTopicsDao
    }
}