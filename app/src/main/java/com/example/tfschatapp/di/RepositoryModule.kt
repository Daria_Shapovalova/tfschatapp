package com.example.tfschatapp.di

import com.example.tfschatapp.database.dao.*
import com.example.tfschatapp.network.ZulipApi
import com.example.tfschatapp.repository.MessagesRepository
import com.example.tfschatapp.repository.SharedPreferencesDao
import com.example.tfschatapp.repository.StreamsRepository
import com.example.tfschatapp.repository.UsersRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Provides
    @Singleton
    fun provideMessagesRepository(
        sharedPreferencesDao: SharedPreferencesDao,
        zulipApi: ZulipApi,
        messageDao: MessageDao,
        reactionDao: ReactionDao,
        messageWithReactionsDao: MessageWithReactionsDao
    ): MessagesRepository {
        return MessagesRepository(sharedPreferencesDao, zulipApi, messageDao, reactionDao, messageWithReactionsDao)
    }

    @Provides
    @Singleton
    fun provideUsersRepository(
        contactsDao: ContactsDao,
        zulipApi: ZulipApi,
        sharedPreferencesDao: SharedPreferencesDao
    ): UsersRepository {
        return UsersRepository(contactsDao, zulipApi, sharedPreferencesDao)
    }

    @Provides
    @Singleton
    fun provideStreamsRepository(
        channelDao: ChannelDao,
        topicDao: TopicDao,
        channelWithTopicsDao: ChannelWithTopicsDao,
        zulipApi: ZulipApi
    ): StreamsRepository {
        return StreamsRepository(channelDao, topicDao, channelWithTopicsDao, zulipApi)
    }
}