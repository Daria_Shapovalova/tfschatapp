package com.example.tfschatapp.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.tfschatapp.database.modeldb.MessageDb
import io.reactivex.rxjava3.core.Single

@Dao
interface MessageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMessages(messages: List<MessageDb>)

    @Query("SELECT uid FROM messages_table WHERE topicId=:topicId AND uid NOT IN (SELECT uid FROM messages_table WHERE topicId=:topicId ORDER BY uid DESC LIMIT :limit)")
    fun getIdsForMessagesInTopicToBeRemoved(topicId: String, limit: Int): Single<List<String>>

    @Query("SELECT uid FROM messages_table WHERE streamName=:streamName AND uid NOT IN (SELECT uid FROM messages_table WHERE streamName=:streamName ORDER BY uid DESC LIMIT :limit)")
    fun getIdsForMessagesInStreamToBeRemoved(streamName: String, limit: Int): Single<List<String>>

    @Query("DELETE FROM messages_table WHERE uid=:messageId")
    fun removeMessage(messageId: String)

    @Query("UPDATE messages_table SET content=:newMessageContent WHERE uid=:messageId")
    fun updateMessageContent(messageId: String, newMessageContent: String)

    @Query("UPDATE messages_table SET topicId=:topicId AND topicName=:topicName WHERE uid=:messageId")
    fun updateMessageTopic(messageId: String, topicId: String, topicName: String)
}