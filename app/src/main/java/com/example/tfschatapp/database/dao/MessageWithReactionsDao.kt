package com.example.tfschatapp.database.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.example.tfschatapp.database.modeldb.MessageWithReactions
import io.reactivex.rxjava3.core.Single

@Dao
interface MessageWithReactionsDao {
    @Transaction
    @Query("SELECT * FROM messages_table WHERE topicId = :topicId")
    fun getMessagesWithReactionsInTopic(topicId: String): Single<List<MessageWithReactions>>

    @Transaction
    @Query("SELECT * FROM messages_table WHERE streamName = :streamName")
    fun getMessagesWithReactionsInStream(streamName: String): Single<List<MessageWithReactions>>
}