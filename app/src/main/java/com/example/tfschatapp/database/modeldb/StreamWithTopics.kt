package com.example.tfschatapp.database.modeldb

import androidx.room.Embedded
import androidx.room.Relation

class StreamWithTopics {
    @Embedded
    lateinit var stream: StreamDb

    @Relation(parentColumn = "uid", entityColumn = "channelId", entity = TopicDb::class)
    var topics: List<TopicDb> = listOf()
}