package com.example.tfschatapp.database.modeldb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tfschatapp.presentation.managers.ColorManager

@Entity(tableName = "topics_table")
data class TopicDb(
    @PrimaryKey
    val uid: String,

    @ColumnInfo
    val name: String,

    @ColumnInfo
    val channelId: String,

    @ColumnInfo
    val messagesCount: Int = 0,

    @ColumnInfo
    val color: Int = ColorManager.getRandomColor()
)