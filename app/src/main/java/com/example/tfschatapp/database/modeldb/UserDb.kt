package com.example.tfschatapp.database.modeldb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tfschatapp.presentation.modelui.UserUi

@Entity(tableName = "contacts_table")
data class UserDb(
    @PrimaryKey
    val uid: String,

    @ColumnInfo
    val name: String,

    @ColumnInfo
    val email: String,

    @ColumnInfo
    val avatarUri: String,

    @ColumnInfo
    val presence: String  = "offline"
) {
    fun toUserUi(): UserUi {
        return UserUi(uid, name, email, avatarUri, presence)
    }
}