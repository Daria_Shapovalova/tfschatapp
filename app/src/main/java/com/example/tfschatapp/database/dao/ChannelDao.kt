package com.example.tfschatapp.database.dao

import androidx.room.*
import com.example.tfschatapp.database.modeldb.StreamDb

@Dao
interface ChannelDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertChannels(stream: List<StreamDb>)

    @Update
    fun updateChannel(stream: StreamDb)

    @Query("UPDATE channels_table SET subscribed = :subscribed WHERE uid = :channelId")
    fun markChannelAsSubscribed(channelId: String, subscribed: Boolean = true)
}