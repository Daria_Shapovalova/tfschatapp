package com.example.tfschatapp.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.tfschatapp.database.modeldb.TopicDb

@Dao
interface TopicDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTopics(topic: List<TopicDb>)

    @Query("UPDATE topics_table SET messagesCount = :unreadCount WHERE uid = :topicUid")
    fun setUnreadMessageCount(topicUid: String, unreadCount: Int)
}