package com.example.tfschatapp.database.modeldb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "messages_table")
data class MessageDb(
    @PrimaryKey
    val uid: String,

    @ColumnInfo
    val date: String,

    @ColumnInfo
    val senderId: String,

    @ColumnInfo
    val senderName: String,

    @ColumnInfo
    val senderEmail: String,

    @ColumnInfo
    val avatarUrl: String,

    @ColumnInfo
    var content: String,

    @ColumnInfo
    var topicId: String,

    @ColumnInfo
    var topicName: String,

    @ColumnInfo
    var streamName: String
)