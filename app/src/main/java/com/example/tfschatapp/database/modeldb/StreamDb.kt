package com.example.tfschatapp.database.modeldb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tfschatapp.presentation.modelui.StreamUi

@Entity(tableName = "channels_table")
data class StreamDb(
    @PrimaryKey
    val uid: String,

    @ColumnInfo
    val name: String,

    @ColumnInfo
    var isOpened: Boolean = false,

    @ColumnInfo
    val subscribed: Boolean = false
) {
    fun toStreamUi(): StreamUi {
        return StreamUi(uid, name, isOpened, subscribed)
    }
}