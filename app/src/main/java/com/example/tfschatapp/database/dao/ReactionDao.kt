package com.example.tfschatapp.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.tfschatapp.database.modeldb.ReactionDb

@Dao
interface ReactionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReaction(reaction: ReactionDb)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReactions(reactions: List<ReactionDb>)

    @Query("DELETE FROM reactions_table WHERE uid = :uid")
    fun deleteReaction(uid: String)

    @Query("DELETE FROM reactions_table WHERE messageId = :messageId")
    fun deleteReactionsForMessage(messageId: String)
}