package com.example.tfschatapp.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.tfschatapp.database.modeldb.UserDb
import io.reactivex.rxjava3.core.Observable

@Dao
interface ContactsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(users: List<UserDb>)

    @Query("SELECT * FROM contacts_table")
    fun getAllUsers(): Observable<List<UserDb>>

    @Query("SELECT * FROM contacts_table WHERE uid = :userId")
    fun getContactById(userId: String): Observable<UserDb>

    @Query("UPDATE contacts_table SET presence = :userPresence WHERE email = :userEmail")
    fun setUsersPresence(userEmail: String, userPresence: String)
}