package com.example.tfschatapp.database.modeldb

import androidx.room.Embedded
import androidx.room.Relation
import com.example.tfschatapp.R
import com.example.tfschatapp.presentation.modelui.*

class MessageWithReactions {
    @Embedded
    lateinit var message: MessageDb

    @Relation(parentColumn = "uid", entityColumn = "messageId", entity = ReactionDb::class)
    lateinit var reactions: List<ReactionDb>

    fun toMessageUi(currentUserId: String): MessageUi {
        val reactionsUi = mutableListOf<ReactionUi>()

        reactions.forEach { reaction ->
            val reactionUi = reactionsUi.find { it.emoji.uid == reaction.emojiName }
            if (reactionUi != null) {
                reactionUi.count++
                if (reaction.userId == currentUserId) {
                    reactionUi.selected = true
                }
            } else {
                reactionsUi.add(
                    ReactionUi(
                        uid = reaction.emojiName,
                        messageId = message.uid,
                        emoji = EmojiUi(reaction.emojiName, reaction.emoji),
                        count = 1,
                        selected = reaction.userId == currentUserId
                    )
                )
            }
        }

        val viewType = if (message.senderId == currentUserId) {
            R.layout.outgoing_message_item
        } else {
            R.layout.incoming_message_item
        }

        return MessageUi(
            uid = message.uid,
            date = DateUi(date = message.date),
            author = UserUi(message.senderId, message.senderName, message.senderEmail, message.avatarUrl),
            messageText = message.content,
            reactions = reactionsUi,
            topicId = message.topicId,
            topicName = message.topicName,
            viewType = viewType
        )
    }
}