package com.example.tfschatapp.database.modeldb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reactions_table")
data class ReactionDb(
    @PrimaryKey
    val uid: String,

    @ColumnInfo
    val emojiName: String,

    @ColumnInfo
    val emoji: String,

    @ColumnInfo
    val messageId: String,

    @ColumnInfo
    val userId: String
)