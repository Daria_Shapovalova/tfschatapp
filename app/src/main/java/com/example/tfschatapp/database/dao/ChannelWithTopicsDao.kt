package com.example.tfschatapp.database.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.example.tfschatapp.database.modeldb.StreamWithTopics
import io.reactivex.rxjava3.core.Observable

@Dao
interface ChannelWithTopicsDao {
    @Transaction
    @Query("SELECT * FROM channels_table")
    fun getChannelsWithTopics(): Observable<List<StreamWithTopics>>
}