package com.example.tfschatapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.tfschatapp.database.dao.*
import com.example.tfschatapp.database.modeldb.*

@Database(entities = [MessageDb::class, ReactionDb::class, UserDb::class, StreamDb::class, TopicDb::class], version = DATABASE_VERSION, exportSchema = false)
abstract class ChatDatabase : RoomDatabase() {
    abstract val messageDao: MessageDao
    abstract val reactionDao: ReactionDao
    abstract val messageWithReactionsDao: MessageWithReactionsDao
    abstract val userDao: ContactsDao
    abstract val channelDao: ChannelDao
    abstract val topicDao: TopicDao
    abstract val channelWithTopicsDao: ChannelWithTopicsDao
}

const val DATABASE_NAME = "tfs_chat_app_database"
private const val DATABASE_VERSION = 54